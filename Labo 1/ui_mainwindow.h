/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *labelTitre;
    QLabel *labelNom;
    QLabel *labelPrenom;
    QLineEdit *lineNom;
    QLineEdit *linePrenom;
    QPushButton *ButtonSuivant;
    QPushButton *ButtonTerminer;
    QLineEdit *lineGroupe;
    QPushButton *ButtonOk;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 190);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        labelTitre = new QLabel(centralWidget);
        labelTitre->setObjectName(QString::fromUtf8("labelTitre"));
        labelTitre->setGeometry(QRect(20, 20, 200, 16));
        labelNom = new QLabel(centralWidget);
        labelNom->setObjectName(QString::fromUtf8("labelNom"));
        labelNom->setGeometry(QRect(30, 60, 60, 13));
        labelPrenom = new QLabel(centralWidget);
        labelPrenom->setObjectName(QString::fromUtf8("labelPrenom"));
        labelPrenom->setGeometry(QRect(30, 90, 60, 13));
        lineNom = new QLineEdit(centralWidget);
        lineNom->setObjectName(QString::fromUtf8("lineNom"));
        lineNom->setGeometry(QRect(110, 60, 230, 20));
        lineNom->setFocusPolicy(Qt::NoFocus);
        lineNom->setReadOnly(true);
        linePrenom = new QLineEdit(centralWidget);
        linePrenom->setObjectName(QString::fromUtf8("linePrenom"));
        linePrenom->setEnabled(false);
        linePrenom->setGeometry(QRect(110, 90, 230, 20));
        linePrenom->setReadOnly(true);
        ButtonSuivant = new QPushButton(centralWidget);
        ButtonSuivant->setObjectName(QString::fromUtf8("ButtonSuivant"));
        ButtonSuivant->setGeometry(QRect(20, 130, 80, 23));
        ButtonTerminer = new QPushButton(centralWidget);
        ButtonTerminer->setObjectName(QString::fromUtf8("ButtonTerminer"));
        ButtonTerminer->setGeometry(QRect(250, 130, 80, 23));
        lineGroupe = new QLineEdit(centralWidget);
        lineGroupe->setObjectName(QString::fromUtf8("lineGroupe"));
        lineGroupe->setGeometry(QRect(230, 20, 43, 20));
        ButtonOk = new QPushButton(centralWidget);
        ButtonOk->setObjectName(QString::fromUtf8("ButtonOk"));
        ButtonOk->setGeometry(QRect(300, 20, 80, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 350, 21));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Deuxieme Dossier Unix 2016", 0, QApplication::UnicodeUTF8));
        labelTitre->setText(QApplication::translate("MainWindow", "Liste des \303\251tudiants du groupe :", 0, QApplication::UnicodeUTF8));
        labelNom->setText(QApplication::translate("MainWindow", "Nom :", 0, QApplication::UnicodeUTF8));
        labelPrenom->setText(QApplication::translate("MainWindow", "Pr\303\251nom :", 0, QApplication::UnicodeUTF8));
        ButtonSuivant->setText(QApplication::translate("MainWindow", "Suivant", 0, QApplication::UnicodeUTF8));
        ButtonTerminer->setText(QApplication::translate("MainWindow", "Terminer", 0, QApplication::UnicodeUTF8));
        ButtonOk->setText(QApplication::translate("MainWindow", "Ok", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
