echo Creation de Ecran.o
g++ -c -m64 -D CPP -D SUN $HOME/Ecran/Ecran.cpp -I $HOME/Ecran

echo Creation de main.o
g++ -c -m64 -D_XOPEN_SOURCE=500 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I. -I/usr/include -I/usr/X11/include -I $HOME/Ecran -o main.o main.cpp

echo Creation de mainwindow.o
g++ -c -m64 -D_XOPEN_SOURCE=500 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I. -I/usr/include -I/usr/X11/include -I $HOME/Ecran -o mainwindow.o mainwindow.cpp

echo Creation de moc_main_window.o
g++ -c -m64 -D_XOPEN_SOURCE=500 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I. -I/usr/include -I/usr/X11/include -I $HOME/Ecran -o moc_mainwindow.o moc_mainwindow.cpp

echo Creation de PremierDossierUnix
g++ -m64 -Wl,-R,/opt/Qt4.8/lib -o PremierDossierUnix main.o  mainwindow.o Ecran.o moc_mainwindow.o    -L/usr/lib/64 -L/usr/X11/lib/64 -L/opt/Qt4.8/lib -lQtGui -L/opt/Qt4.8/lib -L/usr/lib/64 -L/usr/X11/lib/64 -lQtCore -lpthread -lrt

