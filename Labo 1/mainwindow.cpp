#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Ecran.h"

FILE*	hfFich;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lineGroupe->setFocus();
    this->move(400,200);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_ButtonSuivant_clicked()
{
Trace("Dans ButtonSuivant");
 ui->lineNom->setText("Plus d\'étudiants dans le groupe");
}

void MainWindow::on_ButtonTerminer_clicked()
{
    Trace("Dans ButtonTerminer");
    exit(0);
}

void MainWindow::on_ButtonOk_clicked()
{
Trace("Dans ButtonOk");
char	Buff1[255];
strcpy(Buff1,ui->lineGroupe->text().toStdString().c_str());
Trace("--%s--",Buff1);

}
