#include "fenetreclient.h"
#include "ui_fenetreclient.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <signal.h>

#include "Fichier.ini"
#include "Ecran.h"

extern int idQ;
extern FenetreClient* w;

void H(int Sig);

FenetreClient::FenetreClient(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FenetreClient)
{
 ui->setupUi(this);
     
}

FenetreClient::~FenetreClient()
{
    delete ui;
}

void FenetreClient::on_ButtonEmettre_clicked()
{
Trace("Dans on_ButtonEmettre_clicked");

setMessageEmettre("");
}

void FenetreClient::on_ButtonTerminer_clicked()
{
    Trace("Dans on_ButtonTerminer_clicked");

}

void FenetreClient::setMessageRecu(const char* Text)
{
if (strlen(Text) == 0 ) 
   { 
   ui->lineMessageRecu->clear();
   return;
   }
ui->lineMessageRecu->setText(Text);
return;
}

void FenetreClient::setMessageEmettre(const char* Text)
{
if (strlen(Text) == 0 ) 
   { 
   ui->lineMessageEmettre->clear();
   return;
   }
ui->lineMessageEmettre->setText(Text);
return;
}

const char* FenetreClient::getMessageRecu() const
{
char*			Nom = (char*)malloc(255);

if (ui->lineMessageRecu->text().size())
	{ 
	strcpy(Nom,ui->lineMessageRecu->text().toStdString().c_str());
	delete Nom;
	return Nom;
	}
return NULL;
}

const char* FenetreClient::getMessageEmettre() const
{
char*			Nom = (char*)malloc(255);

if (ui->lineMessageEmettre->text().size())
	{ 
	strcpy(Nom,ui->lineMessageEmettre->text().toStdString().c_str());
	delete Nom;
	return Nom;
	}
return NULL;
}

void H(int Sig)
{
Trace("Reception du nsignal %d",Sig);
}
