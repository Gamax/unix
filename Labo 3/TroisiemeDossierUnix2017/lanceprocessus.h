#ifndef LANCEPROCESSUS_H
#define LANCEPROCESSUS_H

#include <QMainWindow>

namespace Ui {
class LanceProcessus;
}

class LanceProcessus : public QMainWindow
{
    Q_OBJECT

public:
    explicit LanceProcessus(QWidget *parent = 0);
    ~LanceProcessus();
    void setNomFich1(const char*);
    const char* getNomFich1() const;
    void setNomFich2(const char*);
    const char* getNomFich2() const;
    void setNomFich3(const char*);
    const char* getNomFich3() const;
    void 	setNbLigneFich1(int);
    void 	setNbLigneFich2(int);
    void 	setNbLigneFich3(int);

private slots:
    void on_ButtonOk_clicked();
    void on_ButtonTerminer_clicked();
    void on_ButtonEffacer_clicked();

private:
    Ui::LanceProcessus *ui;
};

#endif // LANCEPROCESSUS_H
