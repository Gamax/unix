#include "lanceprocessus.h"
#include "ui_lanceprocessus.h"

#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

#include "Ecran.h"

LanceProcessus::LanceProcessus(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LanceProcessus)
{
    ui->setupUi(this);
    struct sigaction Act;
    Act.sa_handler = SIG_DFL;
    sigaction(SIGCHLD,&Act,NULL);
}

LanceProcessus::~LanceProcessus()
{
    delete ui;
}

//##################################################

void LanceProcessus::on_ButtonOk_clicked()
{ 
int Status,id1,id2,id3,ret,CptF = 0;
char NomF1[20];
char NomF2[20];
char NomF3[20];

   Trace("Entree dans ButtonOk");

    strcpy(NomF1,getNomFich1());
    strcpy(NomF2,getNomFich2());
    strcpy(NomF3,getNomFich3());

    id1 = fork();

    if (id1 == 0) { //fils

        execl("./Lect", "Lect", NomF1, (char *) NULL);
        perror("Erreur lors de l'execution");
        exit(1);

    }

    id2 = fork();

    if (id2 == 0) { //fils

        execl("./Lect", "Lect", NomF2, (char *) NULL);
        perror("Erreur lors de l'execution");
        exit(1);

    }

    id3 = fork();

    if (id3 == 0) { //fils

        execl("./Lect", "Lect", NomF3, (char *) NULL);
        perror("Erreur lors de l'execution");
        exit(1);

    }



    for(int i =0;i<3;i++) {
        ret = wait(&Status);
        CptF = WEXITSTATUS(Status);
        if(ret == id1)
            setNbLigneFich1(CptF);
        if(ret == id2)
            setNbLigneFich2(CptF);
        if(ret == id3)
            setNbLigneFich3(CptF);
    }
   
ui->lineNomFich1->setFocus();
}

void LanceProcessus::on_ButtonTerminer_clicked()
{
Trace("Entree dans  ButtonTerminer");
exit(1);
}

//#####################################################

void LanceProcessus::on_ButtonEffacer_clicked()
{
Trace("Entree dans  ButtonEffacer");
setNomFich1("");
setNomFich2("");
setNomFich3("");
setNbLigneFich1(-1);
setNbLigneFich2(-1);
setNbLigneFich3(-1);
}


void LanceProcessus::setNomFich1(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineNomFich1->clear();
   return;
   }
ui->lineNomFich1->setText(Buffer);
}

const char* LanceProcessus::getNomFich1() const
{
if (ui->lineNomFich1->text().size())
	return ui->lineNomFich1->text().toStdString().c_str();
return NULL;
}
void LanceProcessus::setNomFich2(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineNomFich2->clear();
   return;
   }
ui->lineNomFich2->setText(Buffer);
}

const char* LanceProcessus::getNomFich2() const
{
if (ui->lineNomFich2->text().size())
	return ui->lineNomFich2->text().toStdString().c_str();
return NULL;
}
void LanceProcessus::setNomFich3(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineNomFich3->clear();
   return;
   }
ui->lineNomFich3->setText(Buffer);
}

const char* LanceProcessus::getNomFich3() const
{
if (ui->lineNomFich3->text().size())
	return ui->lineNomFich3->text().toStdString().c_str();
return NULL;
}

void LanceProcessus::setNbLigneFich1(int Nb)
{
char	Buffer[20];
if (Nb >= 0)
	{ sprintf(Buffer,"%d",Nb);
     ui->lineNbLigneFich1->setText(Buffer);
     return;
	}
ui->lineNbLigneFich1->clear();
}

void LanceProcessus::setNbLigneFich2(int Nb)
{
char	Buffer[20];
if (Nb >= 0)
	{ sprintf(Buffer,"%d",Nb);
     ui->lineNbLigneFich2->setText(Buffer);
     return;
	}
ui->lineNbLigneFich2->clear();
}

void LanceProcessus::setNbLigneFich3(int Nb)
{
char	Buffer[20];
if (Nb >= 0)
	{ sprintf(Buffer,"%d",Nb);
     ui->lineNbLigneFich3->setText(Buffer);
     return;
	}
ui->lineNbLigneFich3->clear();
}
