/********************************************************************************
** Form generated from reading UI file 'lanceprocessus.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LANCEPROCESSUS_H
#define UI_LANCEPROCESSUS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LanceProcessus
{
public:
    QWidget *centralWidget;
    QLabel *labelPremier;
    QLabel *labelDeuxieme;
    QLabel *labelTroisieme;
    QLabel *labelNom1;
    QLabel *labelNom2;
    QLabel *labelNom3;
    QLineEdit *lineNomFich1;
    QLineEdit *lineNomFich2;
    QLineEdit *lineNomFich3;
    QPushButton *ButtonOk;
    QLabel *labelNb1;
    QLabel *labelNb2;
    QLabel *labelNb3;
    QLineEdit *lineNbLigneFich1;
    QLineEdit *lineNbLigneFich2;
    QLineEdit *lineNbLigneFich3;
    QPushButton *ButtonTerminer;
    QPushButton *ButtonEffacer;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *LanceProcessus)
    {
        if (LanceProcessus->objectName().isEmpty())
            LanceProcessus->setObjectName(QString::fromUtf8("LanceProcessus"));
        LanceProcessus->resize(400, 300);
        centralWidget = new QWidget(LanceProcessus);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        labelPremier = new QLabel(centralWidget);
        labelPremier->setObjectName(QString::fromUtf8("labelPremier"));
        labelPremier->setGeometry(QRect(20, 20, 150, 13));
        QFont font;
        font.setPointSize(8);
        font.setBold(true);
        font.setWeight(75);
        labelPremier->setFont(font);
        labelDeuxieme = new QLabel(centralWidget);
        labelDeuxieme->setObjectName(QString::fromUtf8("labelDeuxieme"));
        labelDeuxieme->setGeometry(QRect(20, 90, 150, 13));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        labelDeuxieme->setFont(font1);
        labelTroisieme = new QLabel(centralWidget);
        labelTroisieme->setObjectName(QString::fromUtf8("labelTroisieme"));
        labelTroisieme->setGeometry(QRect(20, 160, 150, 13));
        labelTroisieme->setFont(font1);
        labelNom1 = new QLabel(centralWidget);
        labelNom1->setObjectName(QString::fromUtf8("labelNom1"));
        labelNom1->setGeometry(QRect(20, 40, 120, 13));
        labelNom2 = new QLabel(centralWidget);
        labelNom2->setObjectName(QString::fromUtf8("labelNom2"));
        labelNom2->setGeometry(QRect(20, 110, 120, 13));
        labelNom3 = new QLabel(centralWidget);
        labelNom3->setObjectName(QString::fromUtf8("labelNom3"));
        labelNom3->setGeometry(QRect(20, 180, 120, 13));
        lineNomFich1 = new QLineEdit(centralWidget);
        lineNomFich1->setObjectName(QString::fromUtf8("lineNomFich1"));
        lineNomFich1->setGeometry(QRect(140, 40, 113, 20));
        lineNomFich2 = new QLineEdit(centralWidget);
        lineNomFich2->setObjectName(QString::fromUtf8("lineNomFich2"));
        lineNomFich2->setGeometry(QRect(140, 110, 113, 20));
        lineNomFich3 = new QLineEdit(centralWidget);
        lineNomFich3->setObjectName(QString::fromUtf8("lineNomFich3"));
        lineNomFich3->setGeometry(QRect(140, 180, 113, 20));
        ButtonOk = new QPushButton(centralWidget);
        ButtonOk->setObjectName(QString::fromUtf8("ButtonOk"));
        ButtonOk->setGeometry(QRect(40, 230, 75, 23));
        labelNb1 = new QLabel(centralWidget);
        labelNb1->setObjectName(QString::fromUtf8("labelNb1"));
        labelNb1->setGeometry(QRect(20, 65, 120, 13));
        labelNb2 = new QLabel(centralWidget);
        labelNb2->setObjectName(QString::fromUtf8("labelNb2"));
        labelNb2->setGeometry(QRect(20, 135, 120, 13));
        labelNb3 = new QLabel(centralWidget);
        labelNb3->setObjectName(QString::fromUtf8("labelNb3"));
        labelNb3->setGeometry(QRect(20, 205, 120, 13));
        lineNbLigneFich1 = new QLineEdit(centralWidget);
        lineNbLigneFich1->setObjectName(QString::fromUtf8("lineNbLigneFich1"));
        lineNbLigneFich1->setGeometry(QRect(140, 65, 50, 20));
        lineNbLigneFich1->setReadOnly(true);
        lineNbLigneFich2 = new QLineEdit(centralWidget);
        lineNbLigneFich2->setObjectName(QString::fromUtf8("lineNbLigneFich2"));
        lineNbLigneFich2->setGeometry(QRect(140, 135, 50, 20));
        lineNbLigneFich2->setReadOnly(true);
        lineNbLigneFich3 = new QLineEdit(centralWidget);
        lineNbLigneFich3->setObjectName(QString::fromUtf8("lineNbLigneFich3"));
        lineNbLigneFich3->setGeometry(QRect(140, 205, 50, 20));
        lineNbLigneFich3->setReadOnly(true);
        ButtonTerminer = new QPushButton(centralWidget);
        ButtonTerminer->setObjectName(QString::fromUtf8("ButtonTerminer"));
        ButtonTerminer->setGeometry(QRect(240, 230, 75, 23));
        ButtonEffacer = new QPushButton(centralWidget);
        ButtonEffacer->setObjectName(QString::fromUtf8("ButtonEffacer"));
        ButtonEffacer->setGeometry(QRect(140, 230, 75, 23));
        LanceProcessus->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(LanceProcessus);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        LanceProcessus->setMenuBar(menuBar);
        statusBar = new QStatusBar(LanceProcessus);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        LanceProcessus->setStatusBar(statusBar);

        retranslateUi(LanceProcessus);

        QMetaObject::connectSlotsByName(LanceProcessus);
    } // setupUi

    void retranslateUi(QMainWindow *LanceProcessus)
    {
        LanceProcessus->setWindowTitle(QApplication::translate("LanceProcessus", "Lance 3 Processus", 0, QApplication::UnicodeUTF8));
        labelPremier->setText(QApplication::translate("LanceProcessus", "Premier Processus:", 0, QApplication::UnicodeUTF8));
        labelDeuxieme->setText(QApplication::translate("LanceProcessus", "Deuxieme processus:", 0, QApplication::UnicodeUTF8));
        labelTroisieme->setText(QApplication::translate("LanceProcessus", "Troisieme Processus:", 0, QApplication::UnicodeUTF8));
        labelNom1->setText(QApplication::translate("LanceProcessus", "Fichier \303\240 traiter:", 0, QApplication::UnicodeUTF8));
        labelNom2->setText(QApplication::translate("LanceProcessus", "Fichier \303\240 traiter:", 0, QApplication::UnicodeUTF8));
        labelNom3->setText(QApplication::translate("LanceProcessus", "Fichier \303\240 traiter:", 0, QApplication::UnicodeUTF8));
        ButtonOk->setText(QApplication::translate("LanceProcessus", "Go", 0, QApplication::UnicodeUTF8));
        labelNb1->setText(QApplication::translate("LanceProcessus", "Nombre de lignes :", 0, QApplication::UnicodeUTF8));
        labelNb2->setText(QApplication::translate("LanceProcessus", "Nombre de lignes :", 0, QApplication::UnicodeUTF8));
        labelNb3->setText(QApplication::translate("LanceProcessus", "Nombre de lignes :", 0, QApplication::UnicodeUTF8));
        ButtonTerminer->setText(QApplication::translate("LanceProcessus", "Terminer", 0, QApplication::UnicodeUTF8));
        ButtonEffacer->setText(QApplication::translate("LanceProcessus", "Effacer", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LanceProcessus: public Ui_LanceProcessus {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LANCEPROCESSUS_H
