#!/usr/bin/env bash
echo Creation de Ecran.o
g++ -c -m64 -o Ecran.o Ecran.cpp -DSUN
 
echo Creation de gereprocessus.o
g++ -c -m64 -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/usr/local/Trolltech/Qt-4.8.6/mkspecs/solaris-g++-64  -I/usr/local/Trolltech/Qt-4.8.6/include/QtCore -I/usr/local/Trolltech/Qt-4.8.6/include/QtGui -I/usr/local/Trolltech/Qt-4.8.6/include -I/usr/X11/include -o gereprocessus.o gereprocessus.cpp

echo Creation de mainGereProcessus.o
g++ -c -m64 -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/usr/local/Trolltech/Qt-4.8.6/mkspecs/solaris-g++-64  -I/usr/local/Trolltech/Qt-4.8.6/include/QtCore -I/usr/local/Trolltech/Qt-4.8.6/include/QtGui -I/usr/local/Trolltech/Qt-4.8.6/include -I/usr/X11/include -o mainGereProcessus.o mainGereProcessus.cpp

echo Creation de moc_gereprocessus.o
g++ -c -m64 -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/usr/local/Trolltech/Qt-4.8.6/mkspecs/solaris-g++-64  -I/usr/local/Trolltech/Qt-4.8.6/include/QtCore -I/usr/local/Trolltech/Qt-4.8.6/include/QtGui -I/usr/local/Trolltech/Qt-4.8.6/include -I/usr/X11/include -o moc_gereprocessus.o moc_gereprocessus.cpp

echo Creation de GereProcessus
g++ -m64 -Wl,-R,/usr/local/Trolltech/Qt-4.8.6/lib -o GereProcessus Ecran.o  gereprocessus.o  mainGereProcessus.o  moc_gereprocessus.o    -L/usr/lib/64 -L/usr/X11/lib/64 -L/usr/local/Trolltech/Qt-4.8.6/lib -lQtGui -L/usr/local/Trolltech/Qt-4.8.6/lib -L/usr/lib/64 -L/usr/X11/lib/64 -lQtCore -lpthread -lrt

echo Creation de mainTraitement.o
g++ -c -m64 -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/usr/local/Trolltech/Qt-4.8.6/mkspecs/solaris-g++-64  -I/usr/local/Trolltech/Qt-4.8.6/include/QtCore -I/usr/local/Trolltech/Qt-4.8.6/include/QtGui -I/usr/local/Trolltech/Qt-4.8.6/include -I/usr/X11/include -o mainTraitement.o mainTraitement.cpp

echo Creation de traitementfichier.o
g++ -c -m64 -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/usr/local/Trolltech/Qt-4.8.6/mkspecs/solaris-g++-64  -I/usr/local/Trolltech/Qt-4.8.6/include/QtCore -I/usr/local/Trolltech/Qt-4.8.6/include/QtGui -I/usr/local/Trolltech/Qt-4.8.6/include -I/usr/X11/include -o traitementfichier.o traitementfichier.cpp

echo Creation de moc_traitementfichier.o
g++ -c -m64 -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/usr/local/Trolltech/Qt-4.8.6/mkspecs/solaris-g++-64  -I/usr/local/Trolltech/Qt-4.8.6/include/QtCore -I/usr/local/Trolltech/Qt-4.8.6/include/QtGui -I/usr/local/Trolltech/Qt-4.8.6/include -I/usr/X11/include -o moc_traitementfichier.o moc_traitementfichier.cpp

echo Creation de TraitementFichier
g++ -m64 -Wl,-R,/usr/local/Trolltech/Qt-4.8.6/lib -o TraitementFichier Ecran.o mainTraitement.o  traitementfichier.o  moc_traitementfichier.o    -L/usr/lib/64 -L/usr/X11/lib/64 -L/usr/local/Trolltech/Qt-4.8.6/lib -lQtGui -L/usr/local/Trolltech/Qt-4.8.6/lib -L/usr/lib/64 -L/usr/X11/lib/64 -lQtCore -lpthread -lrt

