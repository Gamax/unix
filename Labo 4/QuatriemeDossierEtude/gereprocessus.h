#ifndef GEREPROCESSUS_H
#define GEREPROCESSUS_H

#include <QMainWindow>

namespace Ui {
class GereProcessus;
}

class GereProcessus : public QMainWindow
{
    Q_OBJECT

public:
    explicit GereProcessus(QWidget *parent = 0);
    ~GereProcessus();
    void setNomFich1(const char*);
    const char* getNomFich1() const;
    void setNomFich2(const char*);
    const char* getNomFich2() const;
    void setNomFich3(const char*);
    const char* getNomFich3() const;
    void 	setNbLigneFich1(int);
    void 	setNbLigneFich2(int);
    void 	setNbLigneFich3(int);

private slots:
    void on_ButtonTerminer1_clicked();

    void on_ButtonTerminer2_clicked();

    void on_ButtonTerminer3_clicked();

    void on_ButtonGo_clicked();

    void on_ButtonEffacer_clicked();

    void on_ButtonTerminerTout_clicked();

    void on_ButtonTerminer_clicked();

private:
    Ui::GereProcessus *ui;
};

#endif // GEREPROCESSUS_H
