#include "traitementfichier.h"
#include "ui_traitementfichier.h"

#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include "Ecran.h"

FILE*	hfFich;
extern TraitementFichier* w;
extern char Nom[];
extern int Colonne;
extern int	idGrp;

void HandlerAlarm(int);

void HandlerFin(int);

TraitementFichier::TraitementFichier(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TraitementFichier)
{
    ui->setupUi(this);
    this->move(800,Colonne);
    if ((hfFich = fopen(Nom,"r+")) == NULL)
    	exit(1);
    ui->lineEdit->setText(Nom);
    AffTraitementEnCours(Nom);
    struct sigaction Act;
    Act.sa_handler = SIG_IGN;
    sigemptyset(&Act.sa_mask);
    Act.sa_flags = 0;
    sigaction(SIGCHLD,&Act,NULL);
    Act.sa_handler = HandlerAlarm;
    sigemptyset(&Act.sa_mask);
    Act.sa_flags = 0;
    sigaction(SIGALRM,&Act,NULL);
    Act.sa_handler = HandlerFin;
    sigemptyset(&Act.sa_mask);
    Act.sa_flags = 0;
    sigaction(SIGUSR1,&Act,NULL);
    
	alarm(1); 
    
}

TraitementFichier::~TraitementFichier()
{
    delete ui;
}

void TraitementFichier::AffTraitementEnCours(const char* C)
{
ui->lineTraitement->setText(C);
}

void TraitementFichier::AffNumeroTraitement(int Nb)
{
char	B[10];
sprintf(B,"%d",Nb);


this->ui->lineNbTraitement->setText(B);
}
static int Cpt;
void HandlerAlarm(int Sig)
{
char 	B[255];
Trace("Dans HandlerAlarm (%d)",Sig);
if (fgets(B,255,hfFich) == NULL) exit(Cpt);
B[strlen(B)- 1] = '\0';
Cpt++;
w->AffTraitementEnCours(B);
w->AffNumeroTraitement(Cpt);

alarm(1);
}

void HandlerFin(int){
    exit(Cpt);
}

