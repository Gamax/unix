#include "gereprocessus.h"
#include "ui_gereprocessus.h"

#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <iostream>
using namespace std;
#include "Ecran.h"

void FinFils(int);
extern GereProcessus* w;

GereProcessus::GereProcessus(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GereProcessus)
{
    ui->setupUi(this);
    this->move(200,200);
    struct sigaction Act;
    Act.sa_handler = FinFils;
    sigemptyset(&Act.sa_mask);
    Act.sa_flags = 0;
    sigaction(SIGCHLD,&Act,NULL);
    Act.sa_handler = SIG_IGN;
    sigaction(SIGUSR1,&Act,NULL);
//    setNomFich1("aa.txt"); 
//    setNomFich2("bb.txt");
//    setNomFich3("cc.txt");
}

GereProcessus::~GereProcessus()
{
    delete ui;
}
int idF1 = 0;
int idF2 = 0;
int idF3 = 0;

void GereProcessus::on_ButtonTerminer1_clicked()
{
Trace("Terminer 1");
    kill(idF1,SIGUSR1);
}

void GereProcessus::on_ButtonTerminer2_clicked()
{
Trace("Terminer 2");
    kill(idF2,SIGUSR1);
}

void GereProcessus::on_ButtonTerminer3_clicked()
{
Trace("Terminer 3");
    kill(idF3,SIGUSR1);
     
}

void GereProcessus::on_ButtonGo_clicked()
{
    Trace("Dans ButtonGo");

    char NomF1[20];
    char NomF2[20];
    char NomF3[20];

NomF1[0] = '\0'; 

    if (getNomFich1()) {
        strcpy(NomF1, getNomFich1());
        idF1 = fork();
        if (idF1 == 0) {
            execl("./TraitementFichier", "TraitementFichier", NomF1, "200", (char *) NULL);
        }
    }

    if (getNomFich2()) {
        strcpy(NomF2, getNomFich2());
        idF2 = fork();
        if (idF2 == 0) {
            execl("./TraitementFichier", "TraitementFichier", NomF2, "300", (char *) NULL);
        }
    }

    if (getNomFich3()) {
        strcpy(NomF3, getNomFich3());
        idF3 = fork();
        if (idF3 == 0) {
            execl("./TraitementFichier", "TraitementFichier", NomF3, "400", (char *) NULL);
        }
    }
  
ui->lineNomFich1->setFocus();
}

void GereProcessus::on_ButtonEffacer_clicked()
{
Trace("Dans ButtonEffacer");
setNomFich1("");
setNbLigneFich1(-1);
setNomFich2("");
setNbLigneFich2(-1);
setNomFich3("");
setNbLigneFich3(-1);
     
}

void GereProcessus::on_ButtonTerminerTout_clicked()
{
Trace("Dans ButtonTerminerTout");
    kill(0,SIGUSR1);
}

void GereProcessus::on_ButtonTerminer_clicked()
{
Trace("Dans ButtonTerminer");
exit(0);
}

void FinFils(int Sig) {

    int ret,Status,CptF;

    Trace("Reception du signal %d",Sig);

    for(int i =0;i<3;i++) {
        ret = wait(&Status);
        CptF = WEXITSTATUS(Status);
        if(ret == idF1)
            w->setNbLigneFich1(CptF);
        if(ret == idF2)
            w->setNbLigneFich2(CptF);
        if(ret == idF3)
            w->setNbLigneFich3(CptF);
    }


}

void GereProcessus::setNomFich1(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineNomFich1->clear();
   return;
   }
ui->lineNomFich1->setText(Buffer);
}

const char* GereProcessus::getNomFich1() const
{
char*			Nom = (char*)malloc(255);

if (ui->lineNomFich1->text().size())
	{ 
	strcpy(Nom,ui->lineNomFich1->text().toStdString().c_str());
	delete Nom;
	return Nom;
	}
return NULL;
}

void GereProcessus::setNomFich2(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineNomFich2->clear();
   return;
   }
ui->lineNomFich2->setText(Buffer);
}

const char* GereProcessus::getNomFich2() const
{
char*			Nom = (char*)malloc(255);

if (ui->lineNomFich2->text().size())
	{ 
	strcpy(Nom,ui->lineNomFich2->text().toStdString().c_str());
	delete Nom;
	return Nom;
	}
return NULL;
}
void GereProcessus::setNomFich3(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineNomFich3->clear();
   return;
   }
ui->lineNomFich3->setText(Buffer);
}

const char* GereProcessus::getNomFich3() const
{
char*			Nom = (char*)malloc(255);

if (ui->lineNomFich3->text().size())
	{ 
	strcpy(Nom,ui->lineNomFich3->text().toStdString().c_str());
	delete Nom;
	return Nom;
	}
return NULL;
}

void GereProcessus::setNbLigneFich1(int Nb)
{
char	Buffer[20];
if (Nb >= 0)
	{ sprintf(Buffer,"%d",Nb);
     ui->lineNbLigneFich1->setText(Buffer);
     return;
	}
ui->lineNbLigneFich1->clear();
}

void GereProcessus::setNbLigneFich2(int Nb)
{
char	Buffer[20];
if (Nb >= 0)
	{ sprintf(Buffer,"%d",Nb);
     ui->lineNbLigneFich2->setText(Buffer);
     return;
	}
ui->lineNbLigneFich2->clear();
}

void GereProcessus::setNbLigneFich3(int Nb)
{
char	Buffer[20];
if (Nb >= 0)
	{ sprintf(Buffer,"%d",Nb);
     ui->lineNbLigneFich3->setText(Buffer);
     return;
	}
ui->lineNbLigneFich3->clear();
}
