#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
MESSAGE	M;
int hdPipe[2];
void HFin(int);

void HNouvelCommande(int Sig);

int main(int argc, char* argv[])
{

if (argc != 3)
   { Trace("Trop ou trop peu d'argument(s)\n'");
     exit(1);
   }
Trace("Personnel commence");

    hdPipe[0] = atoi(argv[1]);
    hdPipe[1] = atoi(argv[2]);
    if(close(hdPipe[0])) { Trace("Err close Pipe Personnel"); exit(0); } //fermeture lecture

    struct sigaction Action;
Action.sa_handler=HNouvelCommande;
sigemptyset(&Action.sa_mask);
Action.sa_flags=0;
sigaction(SIGUSR1,&Action,NULL);
    Action.sa_handler=HFin;
    sigemptyset(&Action.sa_mask);
    Action.sa_flags=0;
    sigaction(SIGINT,&Action,NULL);

    if ((idMsg = msgget(KEY,0)) == -1)
    { perror("Err. de msgget()");
        exit(1);
    }


while(1)
    pause();



exit(0);
}

void HNouvelCommande(int Sig) //todo modif
{
    Trace("Reception d'une nouvelle commande (%d)",Sig);

    if((msgrcv(idMsg,&M,sizeof(M) - sizeof(long),4,0)) == -1)
    {
        perror("ERREUR: SIGUSR1 non recu\n");
        exit(1);
    }

    FILE* f;
    f = fopen(M.Selection1,"r");
    if(!f)
    {
        perror("\nErr. Open()");

        exit(1);
    }

    char chaine[100],personne[100],commande[100],*pos;
    strcpy(personne,M.Selection2);
    strcpy(commande,M.Selection1);
    M.Type = 1;

    while(fgets(chaine,99,f)){

        pos = strchr(chaine,'\n');
        *pos = '\0';

        Trace("Evolution : %s [%d]",chaine,getpid());

        M.Requete = EVOLUTIONTRAVAIL;
        strcpy(M.Selection2,chaine);
        if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1) //envoi évolution et nom commande
        { perror("Err. de msgsnd()");
            exit(1);
        }

        fgets(chaine,99,f);
        Trace("Sleep %d [%d]",atoi(chaine),getpid());
        sleep(atoi(chaine));


    }

    if(fclose(f))
    {
        perror("\nErr. Close()");
        exit(1);
    }

    strcpy(M.Selection1,commande);
    strcpy(M.Selection2,personne);
    M.Requete = TRAVAILTERMINER;
    if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
        return;


    if(write(hdPipe[1],&M, sizeof(M)) != sizeof(M)){
        Trace("Erreur écriture pipe");
        exit(1);
    }

}

void HFin(int)
{
    close(hdPipe[1]);
    exit(0);
}

