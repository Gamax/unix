// processus ServiceLivraison
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
MESSAGE	M;
int hdPipe[2];
void HFin(int);

char	Buff[255];
int main(int argc, char* argv[])
{

    Trace("Lancement service livraison !");
    if (argc != 3)
    { Trace("Trop ou trop peu d'argument(s)\n'");
        exit(1);
    }

    struct sigaction Action;
    Action.sa_handler=HFin;
    sigemptyset(&Action.sa_mask);
    Action.sa_flags=0;
    sigaction(SIGINT,&Action,NULL);

    hdPipe[0] = atoi(argv[1]);
    hdPipe[1] = atoi(argv[2]);
    if(close(hdPipe[1])) { Trace("Err close Pipe ServiceLivraison"); exit(0); } //fermeture écriture

    if ((idMsg = msgget(KEY,0)) == -1)
    { perror("Err. de msgget()");
        exit(1);
    }


    while(1){
        if(read(hdPipe[0],&M, sizeof(M) ) < 0 ){
            Trace("Erreur Lecture pipe");
            exit(1);
        }

        Trace("Démarrage livraison ---%s--- ---%s---",M.Selection1,M.Selection2);

        sleep(8);

        Trace("Fin livraison ---%s--- ---%s---",M.Selection1,M.Selection2);
        M.Type = 1;
        M.Requete = SUPPRESSIONTRAVAIL;
        if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
        { perror("Err. de msgsnd()");
            exit(1);
        }
    }

pid_t		Pid;
// Recuperation des ressources

Trace("-------------->Debut ");

}

void HFin(int)
{
    close(hdPipe[0]);
    exit(0);
}