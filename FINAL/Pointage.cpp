#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
MESSAGE	M;

int main(int argc, char* argv[])
{
if (argc != 2)
   { printf("Trop ou trop peu d'argument(s)\n'");
     exit(1);
   }
if ((idMsg = msgget(KEY,0)) == -1)
   { perror("Err. de msgget()");
     exit(1);
   }
Trace("Debut");
M.Type = 1;
M.idPid = getpid();
M.Requete = NEWPERSONNEL;
strcpy(M.Selection1,argv[1]);
if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
   { perror("Err. de msgsnd()");
     exit(1);
   }
exit(0);
}
