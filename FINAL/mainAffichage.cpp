#include "windowtableauaffichage.h"
#include <QApplication>
#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
MESSAGE	M;

int	Colonne;
WindowTableauAffichage* w;

int main(int argc, char *argv[])
{
if (argc != 2) 
	{
	Trace("Trop ou trop peu d'argument(s)");
	exit(1);
	}
// recuperation des ressources
	if ((idMsg = msgget(KEY,0)) == -1)
	{ perror("Err. de msgget()");
		exit(1);
	}
// Transmission du pid
	M.Type = 1;
	M.idPid = getpid();
	M.Requete = NEWAFFICHAGE;
	if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
	{ perror("Err. de msgsnd()");
		exit(1);
	}


    
QApplication a(argc, argv);
w = new WindowTableauAffichage;
w->show();

return a.exec();
}
