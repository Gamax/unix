#include "windowadministrateur.h"
#include <QApplication>
#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
MESSAGE	M;
WindowAdministrateur *w;

int main(int argc, char *argv[])
{
// identification de l'adlinistrateur

// recuperation des ressources
 if ((idMsg = msgget(KEY,0)) == -1)
 { perror("Err. de msgget()");
  exit(1);
 }
// Transmission du pid
 M.Type = 1;
 M.idPid = getpid();
 M.Requete = NEWADMIN;
 if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
 { perror("Err. de msgsnd()");
  exit(1);
 }



QApplication a(argc, argv);
w = new WindowAdministrateur;
w->show();

 return a.exec();
}
