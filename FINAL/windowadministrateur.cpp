#include "windowadministrateur.h"
#include "ui_windowadministrateur.h"
#include "Fichier.ini"
#include "Ecran.h"

extern int idMsg;
extern MESSAGE	M;
extern WindowAdministrateur *w;

void HNouvelUtilisateur(int);

WindowAdministrateur::WindowAdministrateur(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::WindowAdministrateur)
{
    ui->setupUi(this);
    move(10,175);
    int i = 0;
    while (i < 4)
    {
        lineCommande[i] = new QLineEdit(this);
        lineCommande[i]->setGeometry(QRect(20,50 + i * 30,120,20));
        lineCommande[i]->setReadOnly(true);
        lineDisponible[i] = new QLineEdit(this);
        lineDisponible[i]->setGeometry(QRect(200,50 + i * 30,120,20));
        lineDisponible[i]->setReadOnly(true);
        i++;
    }

    i = 0;
    while (i < 4)
    {
        linePersonnel[i] = new QLineEdit(this);
        linePersonnel[i]->setGeometry(QRect(450,50 + i * 30,120,20));
        linePersonnel[i]->setReadOnly(true);
        i++;
    }

    lineCommandeN[0] = new QLineEdit(this);
    lineCommandeN[0]->setGeometry(QRect(40,345 ,120,20));
    lineCommandeT[0] = new QLineEdit(this);
    lineCommandeT[0]->setGeometry(QRect(170,345 ,40,20));
    lineCommandeN[1] = new QLineEdit(this);
    lineCommandeN[1]->setGeometry(QRect(280,345 ,120,20));
    lineCommandeT[1] = new QLineEdit(this);
    lineCommandeT[1]->setGeometry(QRect(410,345 ,40,20));
    lineCommandeN[2] = new QLineEdit(this);
    lineCommandeN[2]->setGeometry(QRect(40,375,120,20));
    lineCommandeT[2] = new QLineEdit(this);
    lineCommandeT[2]->setGeometry(QRect(170,375 ,40,20));
    lineCommandeN[3] = new QLineEdit(this);
    lineCommandeN[3]->setGeometry(QRect(280,375 ,120,20));
    lineCommandeT[3] = new QLineEdit(this);
    lineCommandeT[3]->setGeometry(QRect(410,375,40,20));


    // Armement des signaux
    struct sigaction Action;
    Action.sa_handler=HNouvelUtilisateur;
    sigemptyset(&Action.sa_mask);
    Action.sa_flags=0;
    sigaction(SIGUSR1,&Action,NULL);

    setCommande(0,"aaa");
    //setCommande(1,"bbb");
    //setCommande(2,"ccc");
    //setCommande(3,"ddd");
    /*setDisponible(2,"MerceD");
    setDisponible(3,"Defooz");*/

}

#include "FctUtilesAdministrateur.cpp"

WindowAdministrateur::~WindowAdministrateur()
{
    delete ui;
}

void WindowAdministrateur::on_ButtonAccepteSelection_clicked()
{
    Trace("Dans on_ButtonAccepteSelection_clicked");
    if (!getSelectionCommande()) return;
    if (!getSelectionPersonnel()) return;
    Trace("Avant envoyer --%s--%s--\n",getSelectionCommande(),getSelectionPersonnel());

    strcpy(M.Selection1,getSelectionCommande());
    strcpy(M.Selection2,getSelectionPersonnel());
    M.Type = 1;
    M.Requete = NEWCOMMANDE;
    M.idPid = getpid();
    if (msgsnd(idMsg,&M,sizeof(M)-sizeof(long),0) == -1)
    { perror("Err. de msgsnd()");
        exit(1);
    }

    setSelectionCommande("");
    setSelectionPersonnel("");
}

void WindowAdministrateur::on_ButtonAnnulerSelection_clicked()
{
    Trace("Dans on_ButtonAnnulerSelection_clicked");
    //replacer commande

    if(getSelectionCommande() != NULL) {

        for (int i = 0; i < 4; i++) {
            if (getCommande(i) == NULL) {
                setCommande(i, getSelectionCommande());
                setSelectionCommande("");
                i=4;
            }
        }
    }

    if(getSelectionPersonnel() != NULL) {

        //replacer personnel
        for (int i = 0; i < 4; i++) {
            if (getDisponible(i) == NULL) {
                setDisponible(i, getSelectionPersonnel());
                setSelectionPersonnel("");
                i=4;
            }
        }

    }

}

void WindowAdministrateur::on_ButtonAccepterCommande_clicked()
{
    Trace("Dans on_ButtonAccepterCommande_clicked");

    if(getNomCommande() == NULL || getCommandeN(0) == NULL || getCommandeT(0) == -1){
        return;
    }

    FILE* f = fopen(getNomCommande(),"w");


    for(int i=0;i<4 && getCommandeN(i) && getCommandeT(i) != -1;i++){
        fprintf(f,"%s\n%d\n",getCommandeN(i),getCommandeT(i));
    }

    fclose(f);



        for (int i = 0; i < 4; i++) {
            if (getCommande(i) == NULL) {
                setCommande(i, getNomCommande());
                i = 4;
            }
        }

    setNomCommande("");







    return;
}

void WindowAdministrateur::on_ButtonAnnulerCommande_clicked()
{
    printf("Dans on_ButtonAnnulerCommande_clicked\n ");

    setNomCommande("");
}

void WindowAdministrateur::on_ButtonTerminer_clicked()
{
    printf("Dans on_ButtonAnnulerCommande_clicked\n ");
    exit(0);
}

void WindowAdministrateur::on_ButtonCommande1_clicked()
{
    Trace("Dans on_ButtonCommande1_clicked");
    if(getCommande(0) != NULL) {
        if (getSelectionCommande() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getCommande(i) == NULL) {
                    setCommande(i, getSelectionCommande());
                    i = 4;
                }
            }


        }

        setSelectionCommande(getCommande(0));

    }

    setCommande(0,"");

}

void WindowAdministrateur::on_ButtonCommande2_clicked()
{
    Trace("Dans on_ButtonCommande2_clicked");
    if(getCommande(1) != NULL) {
        if (getSelectionCommande() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getCommande(i) == NULL) {
                    setCommande(i, getSelectionCommande());
                    i = 4;
                }
            }


        }
        setSelectionCommande(getCommande(1));
    }

    setCommande(1,"");

}

void WindowAdministrateur::on_ButtonCommande3_clicked()
{
    Trace("Dans on_ButtonCommande3_clicked");
    if(getCommande(2) != NULL) {
        if (getSelectionCommande() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getCommande(i) == NULL) {
                    setCommande(i, getSelectionCommande());
                    i = 4;
                }
            }


        }
        setSelectionCommande(getCommande(2));
    }

    setCommande(2,"");

}

void WindowAdministrateur::on_ButtonCommande4_clicked()
{
    Trace("Dans on_ButtonCommande4_clicked");
    if(getCommande(3) != NULL) {
        if (getSelectionCommande() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getCommande(i) == NULL) {
                    setCommande(i, getSelectionCommande());
                    i = 4;
                }
            }


        }
        setSelectionCommande(getCommande(3));
    }

    setCommande(3,"");

}

void WindowAdministrateur::on_ButtonPersonnel1_clicked()
{
    Trace("Dans on_ButtonPersonnel1_clicked");
    if(getDisponible(0) != NULL) {
        if (getSelectionPersonnel() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getDisponible(i) == NULL) {
                    setDisponible(i, getSelectionPersonnel());
                    i = 4;
                }
            }


        }

        setSelectionPersonnel(getDisponible(0));

    }

    setDisponible(0,"");
}

void WindowAdministrateur::on_ButtonPersonnel2_clicked()
{
    Trace("Dans on_ButtonPersonnel2_clicked");
    if(getDisponible(1) != NULL) {
        if (getSelectionPersonnel() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getDisponible(i) == NULL) {
                    setDisponible(i, getSelectionPersonnel());
                    i = 4;
                }
            }


        }

        setSelectionPersonnel(getDisponible(1));

    }

    setDisponible(1,"");
}

void WindowAdministrateur::on_ButtonPersonnel3_clicked()
{
    Trace("Dans on_ButtonPersonnel3_clicked");
    if(getDisponible(2) != NULL) {
        if (getSelectionPersonnel() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getDisponible(i) == NULL) {
                    setDisponible(i, getSelectionPersonnel());
                    i = 4;
                }
            }


        }

        setSelectionPersonnel(getDisponible(2));

    }

    setDisponible(2,"");
}

void WindowAdministrateur::on_ButtonPersonnel4_clicked()
{
    Trace("Dans on_ButtonPersonnel4_clicked");
    if(getDisponible(3) != NULL) {
        if (getSelectionPersonnel() != NULL) {

            for (int i = 0; i < 4; i++) {
                if (getDisponible(i) == NULL) {
                    setDisponible(i, getSelectionPersonnel());
                    i = 4;
                }
            }


        }

        setSelectionPersonnel(getDisponible(3));

    }

    setDisponible(3,"");
}



void HNouvelUtilisateur(int Sig)
{
    Trace("Reception d'un signal (%d)",Sig);
    int	rc;
        Trace("Message attendu");

        if ((rc = msgrcv(idMsg,&M,sizeof(M) - sizeof(long),2,0)) == -1)
        {
            perror("Erreur de msgrcv\n");
            exit(1);
        }

        Trace("Message lu --%s--",M.Selection1);

        switch(M.Requete)
        {
            case NEWPERSONNEL:
                Trace("NEWPERSONNEL --%s--\n",M.Selection1); //nouveau personnel à ajouter

                for (int i = 0; i < 4; i++) {
                    if (w->getPersonnel(i) == NULL) {
                        w->setPersonnel(i, M.Selection1);
                        i=4;
                    }
                }

                for (int i = 0; i < 4; i++) {
                    if (w->getDisponible(i) == NULL) {
                        w->setDisponible(i, M.Selection1);
                        i=4;
                    }
                }

                return;
            case TRAVAILTERMINER:
                Trace("TRAVAILTERMINER --%s--\n",M.Selection2);

                for (int i = 0; i < 4; i++) {
                    if (w->getDisponible(i) == NULL) {
                        w->setDisponible(i, M.Selection2);
                        i=4;
                    }
                }

                break;

        }
    return;
}
