#include "windowtableauaffichage.h"
#include "ui_windowtableauaffichage.h"

#include "Fichier.ini"
#include "Ecran.h"

extern int Colonne;
extern int idMsg;
extern MESSAGE	M;
extern WindowTableauAffichage* w;

void HNouvelCommande(int);

WindowTableauAffichage::WindowTableauAffichage(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WindowTableauAffichage)
{
    ui->setupUi(this);
    move(Colonne,0);
    int i = 0;
    while (i < 3)
    { lineCommande[i] = new QLineEdit(this);
      lineCommande[i]->setGeometry(QRect(40,20 + i * 30,100,20));
      lineCommande[i]->setReadOnly(true);
      lineCommande[i + 3] = new QLineEdit(this);
      lineCommande[i + 3]->setGeometry(QRect(380,20 + i * 30,100,20));
      lineCommande[i + 3]->setReadOnly(true);
      
      linePersonnel[i] = new QLineEdit(this);
      linePersonnel[i]->setGeometry(QRect(150,20 + i * 30,100,20));
      linePersonnel[i]->setReadOnly(true);
      linePersonnel[i + 3] = new QLineEdit(this);
      linePersonnel[i + 3]->setGeometry(QRect(490,20 + i * 30,100,20));
      linePersonnel[i + 3]->setReadOnly(true);
      
      labelEtat[i] = new QLabel(this);
      labelEtat[i]->setGeometry(QRect(260,20 + i * 30,80,20));
		labelEtat[i + 3] = new QLabel(this);
      labelEtat[i + 3]->setGeometry(QRect(600,20 + i * 30,80,20));
      i++;
    }
// Armement des signaux
    struct sigaction Action;
    Action.sa_handler=HNouvelCommande;
    sigemptyset(&Action.sa_mask);
    Action.sa_flags=0;
    sigaction(SIGUSR2,&Action,NULL);
   
}

WindowTableauAffichage::~WindowTableauAffichage()
{
    delete ui;
}
#include "FctUtilesAffichage.cpp"


void HNouvelCommande(int Sig)
{

    if((msgrcv(idMsg,&M,sizeof(M) - sizeof(long),getpid(),0)) == -1)
    {
        perror("ERREUR: SIGUSR2 non recu\n");
        exit(1);
    }

	switch(M.Requete)
		{
		case NEWCOMMANDE:
		Trace("Reception NEWCOMMANDE");

                for (int i = 0; i < 6; i++) {
                    if (w->getCommande(i) == NULL) {
                        w->setCommande(i, M.Selection1);
                        w->setPersonnel(i,M.Selection2);
                        i=6;
                    }
                }

		break;

	case TRAVAILTERMINER:
		Trace("Reception TRAVAILTERMINER --%s-- --%s--",M.Selection1,M.Selection2);
                for (int i = 0; i < 6; i++) {
                    Trace("i:%d c:%s",i,w->getCommande(i));
                    if (w->getCommande(i) != NULL &&  strcmp(w->getCommande(i), M.Selection1) == 0) {
                        w->setEtat(i, "En Livraison");
                        i = 6;
                    }
                }
                break;

	case EVOLUTIONTRAVAIL:
		Trace("Reception EVOLUTIONTRAVAIL --%s-- --%s--",M.Selection1,M.Selection2);

                for (int i = 0; i < 6; i++) {
                    Trace("i:%d c:%s",i,w->getCommande(i));
                    if (w->getCommande(i) != NULL && strcmp(w->getCommande(i),M.Selection1 )==0) {
                        w->setEtat(i,M.Selection2);
                        i=6;
                    }
                }

		break;
	case SUPPRESSIONTRAVAIL:
		Trace("Reception SUPPRESSIONTRAVAIL --%s-- --%s--",M.Selection1,M.Selection2 );

                for (int i = 0; i < 6; i++) {
                    Trace("i:%d c:%s",i,w->getCommande(i));
                    if (w->getCommande(i) != NULL && strcmp(w->getCommande(i),M.Selection1 )==0) {
                        w->setPersonnel(i,"");
                        w->setCommande(i,"");
                        w->setEtat(i,"");
                        i=6;
                    }
                }

		break;
		}
	
}
