#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <string.h>
#include<time.h>

#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
int idShm;

MESSAGE	M;

TABSERVEUR*	s;
void AfficheTab();
int idFils,idService = -1;
int hdPipe[2];
char arg1[2],arg2[2];

void hFin(int sig) {
	if (s->Serveur1 == getpid())
	{
		s->Serveur1 = 0;
	}
	else
	{
		s->Serveur2 = 0;
	}


	if (!s->Serveur1 && !s->Serveur2)
	{
		printf("\nRemove SHM MQ\n");
		shmctl(idShm,IPC_RMID,0);
		msgctl(idMsg,IPC_RMID,0);
	}



	if(s->tab_personnel[0].pid)
		kill(s->tab_personnel[0].pid,SIGINT);
	if(s->tab_personnel[1].pid)
		kill(s->tab_personnel[1].pid,SIGINT);
	if(s->tab_personnel[2].pid)
		kill(s->tab_personnel[2].pid,SIGINT);
	if(s->tab_personnel[3].pid)
		kill(s->tab_personnel[3].pid,SIGINT);
	if(s->Serveur1 == getpid() && idService)
		kill(idService,SIGINT); //only serv 1
	exit(1);
}

int main(int argc,char* argv[])
{
	int		rc;

	Trace("Serveur lance");

	if ((idMsg = msgget(KEY,IPC_CREAT|IPC_EXCL|0600)) == -1) {
		if ((idMsg = msgget(KEY,0)) == -1)
			exit(1);
		Trace("Erreur déjà file message");

	}

	if ((idShm = shmget(KEY,sizeof(TABSERVEUR),IPC_CREAT|0600)) == -1) {
		if ((idShm = shmget(KEY,sizeof(TABSERVEUR),0)) == -1)
			exit(1);
		Trace("Erreur déjà mem partagée");
	}

	if ((s = (TABSERVEUR*)shmat(idShm,NULL,0)) == NULL) {
		perror("Erreur link mem");
		exit(1);
	}

	struct sigaction sig;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags=0;
	sig.sa_handler=hFin;
	sigaction(SIGINT,&sig,NULL);

	if(s->Serveur1 == 0){ //Premier serv
		s->Serveur1 = getpid();
	}else{
		if(s->Serveur2 == 0){ //Second serv
			s->Serveur2 = getpid();
		}else{
			Trace("Trop de serveurs");
			exit(0);
		}
	}

// Creation des pidus ServiceCommande



// Creation des processus ServiceLivraison
	if(s->Serveur1 == getpid()) {

		if (pipe(hdPipe)) {
			Trace("Pipe error");
			exit(0);
		}

		sprintf(arg1, "%d", hdPipe[0]);
		sprintf(arg2, "%d", hdPipe[1]);

		idService = fork();
		if (idService == 0) {
			execl("ServiceLivraison", "ServiceLivraison", arg1, arg2, (char *) NULL);
		}

// Creation des processus Personnel

		for (int i = 0; i < 4; i++) {
			idFils = fork();
			if (idFils == 0) { //on est ds le fils
				execl("Personnel", "Personnel", arg1, arg2, (char *) NULL);
			} else {
				s->tab_personnel[i].pid = idFils;
				s->tab_personnel[i].pidpointage = 0;
			}
		}

		AfficheTab();

		pause();
	}else {



		int i, j;

		while (1) {
			if ((rc = msgrcv(idMsg, &M, sizeof(M) - sizeof(long), 1, 0)) == -1) {
				exit(1);
			}
			switch (M.Requete) {
				case NEWAFFICHAGE:
					Trace("Message NEWAFFICHAGE");
					for (i = 0; i < 5 && s->Affichage[i] != 0; i++);
					if (i != 5) {
						s->Affichage[i] = M.idPid;
					}
					AfficheTab();
					break;

				case NEWADMIN:
					Trace("Message NEWADMIN");
					s->Administrateur1 = M.idPid;
					AfficheTab();
					break;

				case NEWPERSONNEL:
					Trace("Message NEWPERSONNEL");;
					for (i = 0; i < NBMAX && s->Tab[i].idPid != 0; i++); //rech place
					if (i != NBMAX) {
						strcpy(s->Tab[i].NomUtilisateur, M.Selection1); //Stockage user
						s->Tab[i].idPid = M.idPid;
						M.Type = 2;
						kill(s->Administrateur1, SIGUSR1); //réveiller admin

						if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) //envoyer l'util
						{
							perror("Err. de msgsnd()");
							exit(1);
						}

						for (j = 0; j < NBMAX; j++) {
							if (s->tab_personnel[j].pidpointage == 0) {
								s->tab_personnel[j].pidpointage = M.idPid;
								j = NBMAX;
							}
						}
					}

					AfficheTab();


					break;
				case AJOUTCOMMANDE:
					Trace("Reception AJOUTCOMMANDE");
					break;

				case TRANSMISSIONCOMMANDE:
					Trace("Message TRANSMISSIONCOMMANDE");
					break;

				case NEWCOMMANDE:
					Trace("Message NEWCOMMANDE");
					M.Requete = NEWCOMMANDE; //vers affichage


					for (i = 0; i < 5; i++) {

						if (s->Affichage[i] != 0) {

							M.Type = s->Affichage[i];

							kill(s->Affichage[i], SIGUSR2);//réveiller affichage
							if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
								perror("Err. de msgsnd()");
								exit(1);
							}
						}
					}

					M.Type = 4;//vers personnel

					for (i = 0; i < NBMAX && (strcmp(s->Tab[i].NomUtilisateur, M.Selection2) !=
											  0); i++); //trouver l'util de la tâche
					if (i < NBMAX) {
						for (j = 0; j < NBMAX && s->tab_personnel[j].pidpointage !=
												 s->Tab[i].idPid; j++); //trouver le pid du process Personnel
						if (j < NBMAX) {
							kill(s->tab_personnel[j].pid, SIGUSR1);
							//Trace("Commande envoyée %s %s pid process personnel : %d pid process pointage : %d",M.Selection1,M.Selection2,tab_personnel[j].pid,s->Tab[i].idPid); //todo remove
							if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
								perror("Err. de msgsnd()");
								exit(1);
							}
						}

					}


					break;

				case EVOLUTIONTRAVAIL:
					Trace("Message EVOLUTIONTRAVAIL");

					for (i = 0; i < 5; i++) {

						if (s->Affichage[i] != 0) {

							M.Type = s->Affichage[i];

							kill(s->Affichage[i], SIGUSR2);//réveiller affichage
							if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
								perror("Err. de msgsnd()");
								exit(1);
							}
						}
					}
					break;

				case TRAVAILTERMINER:
					Trace("Message TRAVAILTERMINER");


					for (i = 0; i < 5; i++) {

						if (s->Affichage[i] != 0) {

							M.Type = s->Affichage[i];

							kill(s->Affichage[i], SIGUSR2);//réveiller affichage
							if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
								perror("Err. de msgsnd()");
								exit(1);
							}
						}
					}

					M.Type = 2;

					kill(s->Administrateur1, SIGUSR1);
					if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
						perror("Err. de msgsnd()");
						exit(1);
					}


					break;

				case SUPPRESSIONTRAVAIL:
					Trace("Message SUPPRESSIONTRAVAIL");

					/*M.Type = s->Affichage[0];
                    kill(s->Affichage[0], SIGUSR2);//réveiller affichage
                    if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
                        perror("Err. de msgsnd()");
                        exit(1);
                    }*/

					for (i = 0; i < 5; i++) {

						if (s->Affichage[i] != 0) {

							M.Type = s->Affichage[i];

							kill(s->Affichage[i], SIGUSR2);//réveiller affichage
							if (msgsnd(idMsg, &M, sizeof(M) - 8, 0) == -1) {
								perror("Err. de msgsnd()");
								exit(1);
							}

							Trace("SUPPRESSION TRAVAIL send pid : %d commande %s", s->Affichage[i], M.Selection1);
						}
					}

					break;
			}
//AfficheTab();
		}
	}

// Fin du processus Serveur1
}

void AfficheTab()
{
	int i = 0;
	printf("\tpid du Serveur1 :        %d\n",s->Serveur1);
//printf("\tpid du Serveur1 :      %d\n",s->Serveur2);
	printf("\tpid du Administrateur1 : %d\n",s->Administrateur1);
	printf("\tpid du ServiceCommande : %d\n",s->ServiceCommande);
	printf("\tpid du Affichage :       %d\n",s->Affichage[0]);
	printf("\tTab Utilisateurs:\n");
	while (i < NBMAX)
	{
		printf("\t(i : %d)",i);
		printf("\t%5d -%s-\n",
			   s->Tab[i].idPid,s->Tab[i].NomUtilisateur);
		i++;
	}
	i = 0;
	printf("\tTab Process Personnel:\n");
	while (i < NBMAX)
	{
		printf("\t(i : %d)",i);
		printf("\t%5d %5d\n",
			   s->tab_personnel[i].pid,s->tab_personnel[i].pidpointage);
		i++;
	}
	i=0;
	printf("\tTab Affichage:\n");
	while(i<5){
		printf("\t(i : %d)",i);
		printf("\t%5d\n",s->Affichage[i]);
		i++;
	}

	return ;
}
