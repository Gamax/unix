#!/usr/bin/env bash
echo Création de Ecran.o
g++ -m64 -Wall -I /export/home/student/Ecran -c /export/home/student/Ecran/Ecran.cpp -D SUN

echo Creation de Serveur
g++ -m64 -Wall -I /export/home/student/Ecran -o Serveur Serveur.cpp -I /export/home/student/Ecran Ecran.o

echo Creation de mainAdministrateur.o
g++ -m64 -Wall -I /export/home/student/Ecran -c -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I/export/home/student/Ecran -I. -I. -I/usr/include -I/usr/X11/include -o mainAdministrateur.o mainAdministrateur.cpp

echo Creation de windowadministrateur.o
g++ -m64 -Wall -I /export/home/student/Ecran -c -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I /export/home/student/Ecran -I. -I. -I/usr/include -I/usr/X11/include -o windowadministrateur.o windowadministrateur.cpp

echo Creation de moc_windowadministrateur.o
g++ -m64 -Wall -I /export/home/student/Ecran -c -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I. -I. -I. -I/usr/include -I/usr/X11/include -o moc_windowadministrateur.o moc_windowadministrateur.cpp

echo Creation de Administrateur
g++ -m64 -Wall -I /export/home/student/Ecran -Wl,-R,/opt/Qt4.8/lib -o Administrateur mainAdministrateur.o  windowadministrateur.o Ecran.o  moc_windowadministrateur.o    -L/usr/lib/64 -L/usr/X11/lib/64 -L/opt/Qt4.8/lib -lQtGui -L/opt/Qt4.8/lib -L/usr/lib/64 -L/usr/X11/lib/64 -lQtCore -lpthread -lrt

echo creation de mainAffichage.o
g++ -m64 -Wall -I /export/home/student/Ecran -c -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I /export/home/student/Ecran -I. -I. -I/usr/include -I/usr/X11/include -o mainAffichage.o mainAffichage.cpp

echo creation de windowtableauaffichage.o
g++ -m64 -Wall -I /export/home/student/Ecran -c -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I. -I. -I. -I/usr/include -I/usr/X11/include -o windowtableauaffichage.o windowtableauaffichage.cpp

echo creation de moc_windowtableauaffichage.o
g++ -m64 -Wall -I /export/home/student/Ecran -c -D_XOPEN_SOURCE=600 -D__EXTENSIONS__ -O2 -Wall -W -D_REENTRANT -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -I/opt/Qt4.8/mkspecs/solaris-g++-64 -I. -I/opt/Qt4.8/include/QtCore -I/opt/Qt4.8/include/QtGui -I/opt/Qt4.8/include -I. -I. -I. -I/usr/include -I/usr/X11/include -o moc_windowtableauaffichage.o moc_windowtableauaffichage.cpp

echo creation de Affichage
g++ -m64 -Wall -I /export/home/student/Ecran -Wl,-R,/opt/Qt4.8/lib -o Affichage mainAffichage.o  windowtableauaffichage.o  moc_windowtableauaffichage.o Ecran.o   -L/usr/lib/64 -L/usr/X11/lib/64 -L/opt/Qt4.8/lib -lQtGui -L/opt/Qt4.8/lib -L/usr/lib/64 -L/usr/X11/lib/64 -lQtCore -lpthread -lrt

echo Creation de Personnel
g++ -m64 -Wall -I /export/home/student/Ecran -o Personnel Personnel.cpp -I /export/home/student/Ecran Ecran.o

echo Creation de Pointage
g++ -m64 -Wall -I /export/home/student/Ecran -o Pointage Pointage.cpp -I /export/home/student/Ecran Ecran.o

echo Creation de ServiceLivraison
g++ -m64 -o ServiceLivraison ServiceLivraison.cpp -I /export/home/student/Ecran Ecran.o


