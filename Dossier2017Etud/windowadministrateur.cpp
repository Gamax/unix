#include "windowadministrateur.h"
#include "ui_windowadministrateur.h"
#include "Fichier.ini"
#include "Ecran.h"
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

//typedef struct Save_Commande
//{
//	struct Save_Commande *suiv;
//	char *name;
//}Save_Commande;

//Save_Commande * AjoutListeChainee(Save_Commande *,char *);
void Load();
void Save();
void NewCommand(char*);

extern int idMsg;
extern MESSAGE	M;
extern WindowAdministrateur *w;
int hdFile;
//Save_Commande *pTete,*ajout,*tmp;
char *Tab[5];
void HNouvelUtilisateur(int);
//void Handler_Fin(int);


//void Handler_Fin(int)
//{
//	Save();
//	exit(0);
//}

WindowAdministrateur::WindowAdministrateur(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WindowAdministrateur)
{
    ui->setupUi(this);
    move(10,175);
    int i = 0;
//    int j,rc;
//    char *chaine;
    while (i < 4)
    	{ 
    	lineCommande[i] = new QLineEdit(this);
      lineCommande[i]->setGeometry(QRect(20,50 + i * 30,120,20));
      lineCommande[i]->setReadOnly(true);
      lineDisponible[i] = new QLineEdit(this);
      lineDisponible[i]->setGeometry(QRect(200,50 + i * 30,120,20));
      lineDisponible[i]->setReadOnly(true);
      i++;
      }
      
    i = 0;
    while (i < 4)
		 {
		 linePersonnel[i] = new QLineEdit(this);
		 linePersonnel[i]->setGeometry(QRect(450,50 + i * 30,120,20));
		 linePersonnel[i]->setReadOnly(true);      
		 i++;
		 }
    
   lineCommandeN[0] = new QLineEdit(this);
   lineCommandeN[0]->setGeometry(QRect(40,345 ,120,20));
	lineCommandeT[0] = new QLineEdit(this);
   lineCommandeT[0]->setGeometry(QRect(170,345 ,40,20));
   lineCommandeN[1] = new QLineEdit(this);
   lineCommandeN[1]->setGeometry(QRect(280,345 ,120,20));
	lineCommandeT[1] = new QLineEdit(this);
   lineCommandeT[1]->setGeometry(QRect(410,345 ,40,20));
   lineCommandeN[2] = new QLineEdit(this);
   lineCommandeN[2]->setGeometry(QRect(40,375,120,20));
	lineCommandeT[2] = new QLineEdit(this);
   lineCommandeT[2]->setGeometry(QRect(170,375 ,40,20));
   lineCommandeN[3] = new QLineEdit(this);
   lineCommandeN[3]->setGeometry(QRect(280,375 ,120,20));
	lineCommandeT[3] = new QLineEdit(this);
   lineCommandeT[3]->setGeometry(QRect(410,375,40,20));

    
    // Armement des signaux
    struct sigaction Action;
	Action.sa_handler=HNouvelUtilisateur;
	sigemptyset(&Action.sa_mask);
	Action.sa_flags=0;
	sigaction(SIGUSR1,&Action,NULL);
	
//	Action.sa_handler=Handler_Fin;
//	sigemptyset(&Action.sa_mask);
//	Action.sa_flags=0;
//	sigaction(SIGKILL,&Action,NULL);
	

//    setCommande(0,"aaa");
//    setCommande(1,"bbb");
//    setCommande(2,"ccc");
//    setCommande(3,"ddd");
//    setDisponible(0,"MerceD");
//    setDisponible(1,"Defooz");
	Load();
	if(Tab[0])
	{
		setCommande(0,Tab[0]);
	}
	if(Tab[1])
	{
		setCommande(1,Tab[1]);
	}
	if(Tab[2])
	{
		setCommande(2,Tab[2]);
	}
	if(Tab[3])
	{
		setCommande(3,Tab[3]);
	}
//	if((hdFile = open("Commandes_backup", O_RDONLY,0644))== -1)
//	{
//		perror("\nErr. Open(), fichier inexistant");
//		
//		hdFile = open("Commandes_backup", O_RDONLY|O_CREAT,0644);
//	}
//	else
//	{
//		i = j = 0;
//		chaine = (char*)malloc(sizeof(char));
//		pTete = ajout = tmp = NULL;
//		while((rc=read(hdFile,&chaine[i],sizeof(char)))) //lit chaque caractère et le stock, 1 par 1
//		{
//		
//			if(chaine[i]=='\n' || !rc) //fin de ligne
//			{
//				i = 0;
////				pTete = AjoutListeChainee(pTete,chaine);
//				ajout = malloc(sizeof(struct Save_Commande));
//				strcpy(ajout->name,chaine);
//				ajout->suiv = NULL;
//				if(pTete == NULL)
//				{
//					pTete = ajout;
//				}
//				else
//				{
//					tmp = pTete;
//					while(tmp->suiv != NULL)
//					{
//						tmp = tmp->suiv;
//					}
//					tmp->suiv = ajout;
//				}
//				ajout = NULL;
//			}
//	}
	
	//close(hdFile);
}

void Load()
{
	int rc,i,j;
	char *chaine,*tmp;
	chaine = (char*)malloc(sizeof(char));
	tmp = (char*)malloc(sizeof(char));
	if((hdFile = open("Commandes_backup", O_RDONLY,0644))== -1)
	{
		perror("\nErr. Open(), fichier inexistant");
		
		hdFile = open("Commandes_backup", O_RDWR|O_CREAT|O_TRUNC,0644);
		
		strcpy(tmp,"aaa~");
		write(hdFile,tmp,4*sizeof(char));
		strcpy(tmp,"bbb~");
		write(hdFile,tmp,4*sizeof(char));
		strcpy(tmp,"ccc~");
		write(hdFile,tmp,4*sizeof(char));
//		strcpy(tmp,"ddd|");
//		write(hdFile,tmp,4*sizeof(char));
//		write(hdFile,'\n',sizeof(char));
//		write(hdFile,"bbb",3*sizeof(char));
		Tab[0] = (char*)malloc(sizeof(char));
		strcpy(Tab[0],"aaa");
		Tab[1] = (char*)malloc(sizeof(char));
		strcpy(Tab[1],"bbb");
		Tab[2] = (char*)malloc(sizeof(char));
		strcpy(Tab[2],"ccc");
	}
	else
	{
		i = j = 0;
		while((rc=read(hdFile,&chaine[i],sizeof(char)))) //lit chaque caractère et le stock, 1 par 1
		{
			if(chaine[i]=='~' || !rc) //fin de nom
			{
				chaine[i] = '\0';
				Trace("rc = %d i = %d Lu : %s\n",rc,i,chaine);
//				i=0;
				if(j<4)
				{
					Tab[j] = (char*)malloc(sizeof(char));
					strcpy(Tab[j],chaine);
					j++;
				}
				for(;i>0;i--) chaine[i] = '\0';
			}
			else
			{
				i++;
			}
			
		}
//		Trace("%d\n",chaine);
	}
	
	close(hdFile);
}

void Save()
{
	char chaine;
	chaine = '~';
	if((hdFile = open("Commandes_backup", O_WRONLY|O_TRUNC,0644))!= -1)
	{
		Trace("?");
		if(w->getCommande(0))
		{
			Trace("0");
			write(hdFile,w->getCommande(0),sizeof(w->getCommande(0)));
			write(hdFile,&chaine,sizeof(char));
		}
		if(w->getCommande(1))
		{
			Trace("1");
			write(hdFile,w->getCommande(1),sizeof(w->getCommande(1)));
			write(hdFile,&chaine,sizeof(char));
		}
		if(w->getCommande(2))
		{
			Trace("2");
			write(hdFile,w->getCommande(2),sizeof(w->getCommande(2)));
			write(hdFile,&chaine,sizeof(char));
		}
		if(w->getCommande(3))
		{
			Trace("3");
			write(hdFile,w->getCommande(3),sizeof(w->getCommande(3)));
			write(hdFile,&chaine,sizeof(char));
		}
	}
	Trace("Fin %d",hdFile);
	close(hdFile);
}
//Save_Commande * AjoutListeChainee(Save_Commande *pTete,char *chaine)
//{
//	Save_Commande *ajout = malloc(sizeof(Save_Commande));
//	
//	strcpy(ajout->name,chaine);
//	ajout->suiv = NULL;
//	
//	if(pTete == NULL)
//	{
//		return ajout;
//	}
//	
//	Save_Commande *tmp = pTete;
//	while(tmp->suiv != NULL)
//	{
//		tmp = tmp->suiv;
//	}
//	tmp->suiv = ajout;
//	return pTete;
//}

#include "FctUtilesAdministrateur.cpp"

WindowAdministrateur::~WindowAdministrateur()
{
    delete ui;
}

void WindowAdministrateur::on_ButtonAccepteSelection_clicked()
{
Trace("Dans on_ButtonAccepteSelection_clicked");
if (!getSelectionCommande()) return;
if (!getSelectionPersonnel()) return;
Trace("Lu --%s--%s--\n",getSelectionCommande(),getSelectionPersonnel());

strcpy(M.Selection1,getSelectionCommande());
strcpy(M.Selection2,getSelectionPersonnel());
M.Type = 1L;
M.Requete = NEWCOMMANDE;
M.idPid = getpid();
if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
{ perror("Err. de msgsnd()");
	exit(1);
}
setSelectionCommande("");
setSelectionPersonnel("");
}

void WindowAdministrateur::on_ButtonAnnulerSelection_clicked()
{
//char *chaine;
Trace("Dans on_ButtonAnnulerSelection_clicked");
if(getSelectionCommande())
{
	if(!getCommande(0))
	{
		setCommande(0,getSelectionCommande());
		setSelectionCommande("");
	}
	else
	{
		if(!getCommande(1))
		{
			setCommande(1,getSelectionCommande());
			setSelectionCommande("");
		}
		else
		{
			if(!getCommande(2))
			{
				setCommande(2,getSelectionCommande());
				setSelectionCommande("");
			}
			else
			{
				if(!getCommande(3))
				{
					setCommande(3,getSelectionCommande());
					setSelectionCommande("");
				}
			}
		}
	}
}



if(getSelectionPersonnel())
{
	if(!getDisponible(0))
	{
		setDisponible(0,getSelectionPersonnel());
		setSelectionPersonnel("");
	}
	else
	{
		if(!getDisponible(1))
		{
			setDisponible(1,getSelectionPersonnel());
			setSelectionPersonnel("");
		}


		else
		{
			if(!getDisponible(2))
			{
				setDisponible(2,getSelectionPersonnel());
				setSelectionPersonnel("");
			}
			else
			{
				if(!getDisponible(3))
				{
					setDisponible(3,getSelectionPersonnel());
					setSelectionPersonnel("");
				}
			}
		}
	}
}

}

void NewCommand(char*chaine)
{
	char *separation;
	char *chiffre;
//	int i=0;
	if((hdFile = open(chaine, O_RDWR|O_TRUNC,0644))== -1)
	{
		close(hdFile);
		if((hdFile = open(chaine, O_RDWR|O_CREAT,0644))== -1)
		{
			Trace("ERREUR CREATION FICHIER COMMANDE");
			return;
		}
	}
	
	
	separation = (char*)malloc(sizeof(char));
	chiffre = (char*)malloc(sizeof(char));
//	sprintf(chiffre,"%s~%d~%s~%d~%s~%d~%s~%d~",w->getCommandeN(0),w->getCommandeT(0),w->getCommandeN(1),w->getCommandeT(1),w->getCommandeN(2),w->getCommandeT(2),w->getCommandeN(3),w->getCommandeT(3));
//	write(hdFile,chiffre,sizeof(chaine));
	strcpy(separation,"~");
	if(w->getCommandeN(0) && w->getCommandeT(0))
	{
		Trace("Etape 0");
		
			write(hdFile,w->getCommandeN(0),sizeof(w->getCommandeN(0)));
			write(hdFile,separation,sizeof(separation));
			sprintf(chiffre,"%d",w->getCommandeT(0));
			write(hdFile,chiffre,sizeof(chiffre));
			write(hdFile,separation,sizeof(separation));
			
	}
	if(w->getCommandeN(1) && w->getCommandeT(1))
	{
		Trace("Etape 1");
			write(hdFile,w->getCommandeN(1),sizeof(w->getCommandeN(1)));
			write(hdFile,separation,sizeof(separation));
			sprintf(chiffre,"%d",w->getCommandeT(0));
			write(hdFile,chiffre,sizeof(chiffre));
			write(hdFile,separation,sizeof(separation));
			
	}
	if(w->getCommandeN(2) && w->getCommandeT(2))
	{
		Trace("Etape 2");
			write(hdFile,w->getCommandeN(2),sizeof(w->getCommandeN(2)));
			write(hdFile,separation,sizeof(separation));
			sprintf(chiffre,"%d",w->getCommandeT(0));
			write(hdFile,chiffre,sizeof(chiffre));
			write(hdFile,separation,sizeof(separation));
			
	}
	if(w->getCommandeN(3) && w->getCommandeT(3))
	{
		Trace("Etape 3");
			write(hdFile,w->getCommandeN(3),sizeof(w->getCommandeN(3)));
			write(hdFile,separation,sizeof(separation));
			sprintf(chiffre,"%d",w->getCommandeT(0));
			write(hdFile,chiffre,sizeof(chiffre));
			write(hdFile,separation,sizeof(separation));
			
	}
	close(hdFile);
}

void WindowAdministrateur::on_ButtonAccepterCommande_clicked()
{
	char *chaine;
	
	Trace("Dans on_ButtonAccepterCommande_clicked");
	if(getNomCommande())
	{
		chaine = (char*)malloc(sizeof(char));
		
		strcpy(chaine,getNomCommande());
		if(!getCommande(0))
		{
			setCommande(0,getNomCommande());
			setNomCommande("");
		}
		else
		{
			if(!getCommande(1))
			{
				setCommande(1,getNomCommande());
				setNomCommande("");
			}
			else
			{
				if(!getCommande(2))
				{
					setCommande(2,getNomCommande());
					setNomCommande("");
				}
				else
				{
					if(!getCommande(3))
					{
						setCommande(3,getNomCommande());
						setNomCommande("");
					}
				}
			}
		}
		NewCommand(chaine);
	}
return;
}

void WindowAdministrateur::on_ButtonAnnulerCommande_clicked()
{
	printf("Dans on_ButtonAnnulerCommande_clicked\n ");
	setNomCommande("");
	setCommandeN(0,"");
	setCommandeN(1,"");
	setCommandeN(2,"");
	setCommandeN(3,"");
	setCommandeT(0,"");
	setCommandeT(1,"");
	setCommandeT(2,"");
	setCommandeT(3,"");
}

void WindowAdministrateur::on_ButtonTerminer_clicked()
{
printf("Dans on_ButtonTerminerCommande_clicked\n ");
Save();
exit(0);
}

void WindowAdministrateur::on_ButtonCommande1_clicked()
{
Trace("Dans on_ButtonCommande1_clicked");
if(getCommande(0) && !getSelectionCommande())
{
	setSelectionCommande(getCommande(0));
	setCommande(0,"");
}

}

void WindowAdministrateur::on_ButtonCommande2_clicked()
{
Trace("Dans on_ButtonCommande2_clicked");
if(getCommande(1) && !getSelectionCommande())
{
	setSelectionCommande(getCommande(1));
	setCommande(1,"");
}
}

void WindowAdministrateur::on_ButtonCommande3_clicked()
{
Trace("Dans on_ButtonCommande3_clicked");
if(getCommande(2) && !getSelectionCommande())
{
	setSelectionCommande(getCommande(2));
	setCommande(2,"");
}
}

void WindowAdministrateur::on_ButtonCommande4_clicked()
{
Trace("Dans on_ButtonCommande4_clicked");
if(getCommande(3) && !getSelectionCommande())
{
	setSelectionCommande(getCommande(3));
	setCommande(3,"");
}
}

void WindowAdministrateur::on_ButtonPersonnel1_clicked()
{
Trace("Dans on_ButtonPersonnel1_clicked");
if(getDisponible(0) && !getSelectionPersonnel())
{
	setSelectionPersonnel(getDisponible(0));
	setDisponible(0,"");
}
}

void WindowAdministrateur::on_ButtonPersonnel2_clicked()
{
Trace("Dans on_ButtonPersonnel2_clicked");
if(getDisponible(1) && !getSelectionPersonnel())
{
	setSelectionPersonnel(getDisponible(1));
	setDisponible(1,"");
}
}

void WindowAdministrateur::on_ButtonPersonnel3_clicked()
{
Trace("Dans on_ButtonPersonnel3_clicked");

if(getDisponible(2) && !getSelectionPersonnel())
{
	setSelectionPersonnel(getDisponible(2));
	setDisponible(2,"");
}
}

void WindowAdministrateur::on_ButtonPersonnel4_clicked()
{
Trace("Dans on_ButtonPersonnel4_clicked");

if(getDisponible(3) && !getSelectionPersonnel())
{
	setSelectionPersonnel(getDisponible(3));
	setDisponible(3,"");
}
}



void HNouvelUtilisateur(int Sig)
{
Trace("Reception d'un signal (%d)",Sig);
int	rc;
//while (1 )
//   {  



if ((rc = msgrcv(idMsg,&M,sizeof(M) - sizeof(long),1,0)) == -1)
{
	perror("ERREUR: SIGUSR1 non recu\n");
	exit(1);
}

switch(M.Requete)
{
case NEWPERSONNEL:
	Trace("Message reçu NEWPERSONNEL");
	Trace("Message lu --%s--",M.Selection1);
	
	if(!w->getPersonnel(0))
	{
		w->setPersonnel(0,M.Selection1);
	}
	else
	{
		if(!w->getPersonnel(1))
		{
			w->setPersonnel(1,M.Selection1);
		}
		else
		{
			if(!w->getPersonnel(2))
			{
				w->setPersonnel(2,M.Selection1);
			}
			else
			{
				if(!w->getPersonnel(3))
				{
					w->setPersonnel(3,M.Selection1);
				}
			}
		}
	}
	
	if(!w->getDisponible(0))
	{
		w->setDisponible(0,M.Selection1);
	}
	else
	{
		if(!w->getDisponible(1))
		{
			w->setDisponible(1,M.Selection1);
		}
		else
		{
			if(!w->getDisponible(2))
			{
				w->setDisponible(2,M.Selection1);

			}
			else
			{
				if(!w->getDisponible(3))
				{
					w->setDisponible(3,M.Selection1);

				}
			}
		}
	}
	
//	for(rc=0;rc<NBMAX && w->getPersonnel(rc);rc++);
//	if(rc!=NBMAX)
//	{
//		Trace("%d",rc);
//		w->setPersonnel(rc,M.Selection1);
//	}
//	else
//	{
//		Trace("Erreur NEWPERSONNEL (pas de place libre)");
//	}
	break;
	case TRAVAILTERMINER:
		Trace("TRAVAILTERMINER --%s--\n",M.Selection1);

		if(!w->getDisponible(0))
		{
			w->setDisponible(0,M.Selection1);
		}
		else
		{
			if(!w->getDisponible(1))
			{
				w->setDisponible(1,M.Selection1);

			}
			else
			{
				if(!w->getDisponible(2))
				{
					w->setDisponible(2,M.Selection1);
				}
				else
				{
					if(!w->getDisponible(3))
					{
						w->setDisponible(3,M.Selection1);
					}
				}
			}
}
		
		break;
}

   
   
//	switch(M.Requete)
//	{
//	case NEWPERSONNEL:
//	Trace("NEWPERSONNEL --%s--\n",M.Selection1);

//   break;

//	case TRAVAILTERMINER:
//	Trace("TRAVAILTERMINER --%s--\n",M.Selection2);

//   break;
   
//   }
//	}
return;
}
