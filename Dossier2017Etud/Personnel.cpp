#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <setjmp.h>
#include "Fichier.ini"
#include "Ecran.h"

int idMsg;
MESSAGE	M;
jmp_buf env;
int hdPipe[2];
void HNouvelCommande(int);

int main(int argc, char* argv[])
{

//if (argc != 3)
//   { Trace("Trop ou trop peu d'argument(s)\n'");
//     exit(1);
//   }
	hdPipe[0] = atoi(argv[1]);
	hdPipe[1] = atoi(argv[2]);
	if(close(hdPipe[0])) { Trace("Erreur de close pipe PERS"); exit(0); } //fermeture côté lecture inutile
	struct sigaction Action;
	Action.sa_handler=HNouvelCommande;
	sigemptyset(&Action.sa_mask);
	Action.sa_flags=0;
	sigaction(SIGINT,&Action,NULL);

//Trace("%s commence",argv[0]);
	sleep(1);
	if ((idMsg = msgget(KEY,0)) == -1)
	{ perror("Err. de msgget()");
		exit(1);
	}

//sigsetjmp(env,0);
//pause();


//Trace("%s fin",argv[0]);
//exit(0);

	char *chaine,*personne,*commande;
	int rc, File, i,b=0;
	
	//Trace("Signal recu [PERSONNEL]");
	
	
	while(1)
	{
		Trace("%s commence (%d)",argv[0],getpid());
		if ((rc = msgrcv(idMsg,&M,sizeof(M) - sizeof(long),getpid(),0)) == -1)
		{
			Trace("Exit [%s]",argv[0]);
			exit(1);
		}
		Trace("Ouverture fichier [%s]",argv[0]);
		if((File = open(M.Selection1, O_RDWR,0644))== -1)
		{
			perror("\nErr. Open()");

			exit(1);
		}
		M.Type = 1L;
		chaine = (char*)malloc(sizeof(char));
		personne = (char*)malloc(sizeof(char));
		commande = (char*)malloc(sizeof(char));
		strcpy(personne,M.Selection2);
		strcpy(commande,M.Selection1);
		i=0;
		Trace("Lecture [%s]",argv[0]);
		while((rc=read(File,&chaine[i],sizeof(char)))) //lit chaque caractère et le stock, 1 par 1
		{
			Trace("%d | %c\n",chaine[i],chaine[i]);
			if(chaine[i]=='~' || !rc) //fin de ligne
			{
				chaine[i] = '\0';
				if(b) //lignes pairs
				{
					Trace("Sleep %d [%s]",atoi(chaine),argv[0]);
					sleep(atoi(chaine));
					b--;
					for(;i>0;i--) chaine[i] = '\0';
				}
				else //lignes impair
				{
					Trace("Evolution : %s [%s]",chaine,argv[0]);
				
					M.Requete = EVOLUTIONTRAVAIL;
					strcpy(M.Selection2,chaine);
				
					if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1) //envoit nom commande, statut en cours
					{ perror("Err. de msgsnd()");
						exit(1);
					}
					for(;i>0;i--) chaine[i] = '\0';
					b++;
				}
			}
			else
			{
				i++;
			}
		
		}
		Trace("Fin [%s]",argv[0]);
		//fin traitrement et fermeture du fichier
		if(close(File))
		{
			perror("\nErr. Close()");
			exit(1);
		}
	
		strcpy(M.Selection2,commande);
		strcpy(M.Selection1,personne);
		M.Requete = TRAVAILTERMINER;
		M.Type = 1L;
		if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1) exit(0);//envoit comme quoi il a fini
		
		sleep(2);
		Trace("Envoi pipe");
		strcpy(M.Selection2,commande);
		if(write(hdPipe[1],&M,sizeof(M)) != sizeof(M)) {Trace("Erreur envoit dans Pipe"); }

	}
}

void HNouvelCommande(int)
{
	close(hdPipe[1]);
	exit(0);
}
