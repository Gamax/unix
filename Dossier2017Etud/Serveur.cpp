/**********************************************************************
			Serveur.cpp UNIX 2017-2018
			Janvier, CRABBE Pierre-Alexandre 2221
**********************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <string.h>
#include<time.h>

#include "Fichier.ini"
#include "Ecran.h"

typedef struct
{
	int Occupe; // 0 si libre, 1 si occupé.
	pid_t Process; //pid du process
	pid_t Personne; //pid du processus Pointage qui a amené le membre du personnel
}Link_Tableau_Element;


void Handler_clean(int);

int idMsg;
int idShm;

MESSAGE	M;

TABSERVEUR*	s;
void AfficheTab();
int idFils;
pid_t ServiceLiv;
int hdPipe[2];
char argv1[2],argv2[2];
Link_Tableau_Element Personnels[4];
	
	
void handler_clean(int sig)					//Nettoye les sémaphores et la mémoire partagée
{	
//	printf("\nSIGINT\n");
	if (s->Serveur1 != 0)
	{
		s->Serveur1 = 0;
	}
	else 
	{
		s->Serveur2 = 0;
	}
	
	
	if (!s->Serveur1 && !s->Serveur2)
	{
		printf("\nRemove SHM MQ\n");
		shmctl(idShm,IPC_RMID,0);
		msgctl(idMsg,IPC_RMID,0);
	}
	
	if(Personnels[0].Process) kill(Personnels[0].Process,SIGINT);
	if(Personnels[1].Process) kill(Personnels[1].Process,SIGINT);
	if(Personnels[2].Process) kill(Personnels[2].Process,SIGINT);
	if(Personnels[3].Process) kill(Personnels[3].Process,SIGINT);
	if(ServiceLiv) kill(ServiceLiv,SIGINT);
	exit(1);
}
	
	
int main(int argc,char* argv[])
{
	int		rc;
	int i,j;


	Trace("Serveur lance");
	s = (TABSERVEUR*)malloc(sizeof(TABSERVEUR));

	if ((idMsg = msgget(KEY,IPC_CREAT|IPC_EXCL|0600)) == -1) //Armement de la file de message
	{
		if ((idMsg = msgget(KEY,0)) == -1)
			exit(1);
		Trace("La File existe déjà");
	}

	if ((idShm = shmget(KEY,sizeof(TABSERVEUR),IPC_CREAT|0600)) == -1)
	{
		if ((idShm = shmget(KEY,sizeof(TABSERVEUR),0)) == -1) exit(1);
		Trace("La Memoire partagée existe déjà");
	}
	
	if ((s = (TABSERVEUR*)shmat(idShm,NULL,0)) == (TABSERVEUR*)-1)
	{
	perror("ERREUR: Impossible d'attacher la memoire'\n");
	exit(1);
	}
	
	if(s->Serveur1 == 0) {	
	s->Serveur1 = getpid(); 
	}
	else {
		
		if(s->Serveur2 == 0) {
	 	s->Serveur2 = getpid();
		}else{ 
	  Trace("Serveur Refuse");
	   exit(0); 
	   } 
	}

	struct sigaction sig;
	sigemptyset(&sig.sa_mask);
	sigaddset(&sig.sa_mask,SIGUSR1);
	sig.sa_flags=0;
	sigprocmask(SIG_SETMASK,&sig.sa_mask,NULL);

	sig.sa_handler=handler_clean;
	sigaction(SIGINT,&sig,NULL);
	
	for(i=0;i<5;i++) s->Affichage[i]=0;

	
	if(pipe(hdPipe)) {Trace("Erreur Pipe création"); exit(0);}
	Trace("Pipe crée");
	sprintf(argv1,"%d",hdPipe[0]);
	sprintf(argv2,"%d",hdPipe[1]);
	// Creation des processus ServiceCommande

	// Creation des processus ServiceLivraison
	idFils = fork();
	if(!idFils)
	{
		execlp("ServiceLivraison","ServiceLivraison",argv1,argv2,(char*)NULL);
	}
	else
	{
		ServiceLiv = idFils;
	}
	// Creation des processus Personnel
	idFils = fork();
	if(!idFils) //fils
	{
		execlp("Personnel","Personnel_0",argv1,argv2,(char*)NULL);
	}
	else
	{
		Personnels[0].Process = idFils;
		Personnels[0].Occupe = 0;
	
		idFils = fork();
		if(!idFils) //fils
		{
			execlp("Personnel","Personnel_1",argv1,argv2,(char*)NULL);
		}
		else
		{
			Personnels[1].Process = idFils;
			Personnels[1].Occupe = 0;
		
			idFils = fork();
			if(!idFils) //fils
			{
				execlp("Personnel","Personnel_2",argv1,argv2,(char*)NULL);
			}
			else
			{
				Personnels[2].Process = idFils;
				Personnels[2].Occupe = 0;
			
				idFils = fork();
				if(!idFils) //fils
				{
					execlp("Personnel","Personnel_3",argv1,argv2,(char*)NULL);
				}
				else
				{
					Personnels[3].Process = idFils;
					Personnels[3].Occupe = 0;
				
				}
			}
		}
	}

	AfficheTab();
		   
	while(1)
	{
		if ((rc = msgrcv(idMsg,&M,sizeof(M) - sizeof(long),1,0)) == -1)
		{
			exit(1);
		}
		switch(M.Requete)
			{
			case NEWAFFICHAGE:
				Trace("Message NEWAFFICHAGE");
				for(i=0;i<5 && s->Affichage[i]!=0;i++);
				if(i<5)
				{
					s->Affichage[i] = M.idPid;
				}
				//AfficheTab();
				printf("i = %d",i);
				break;
			
			case NEWADMIN:	
				Trace("Message NEWADMIN");
				s->Administrateur1 = M.idPid;
				break;
			
			case NEWPERSONNEL:
				Trace("Message NEWPERSONNEL (%s)",M.Selection1);
				for (i=0;i<NBMAX && s->Tab[i].idPid != 0; i++); //Recherche d'une place libre
				if(i!=NBMAX) 
				{
					strcpy(s->Tab[i].NomUtilisateur,M.Selection1); //On insère le Username
					s->Tab[i].idPid = M.idPid;
					kill(s->Administrateur1,SIGUSR1); //Signale l'Admin d'un nouveau pointage'

					if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
					{ perror("Err. de msgsnd()");
						exit(1);
					}
				
					for(i=0;i<4 && Personnels[i].Personne;i++);
					if(i<4)
					{
						Personnels[i].Personne = M.idPid;
					}
					else
					{
						Trace("Erreur lien avec process Personnel");
					}
					sleep(1);
				}
				else { Trace("Tentative de connexion échouée, pas de place libre."); }
				break;
			case AJOUTCOMMANDE:
				Trace("Reception AJOUTCOMMANDE");
				break;
			
			case TRANSMISSIONCOMMANDE:
				Trace("Message TRANSMISSIONCOMMANDE");
				break;
			
			case NEWCOMMANDE:
				Trace("Message NEWCOMMANDE");
			
					M.Requete = NEWCOMMANDE; //transmission à affichage
					for(i=0;i<5;i++)
					{
						if(s->Affichage[i]!=0)
						{
							kill(s->Affichage[i],SIGUSR1);
							M.Type = s->Affichage[i];
							if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
							{ perror("Err. de msgsnd()");
								exit(1);
							}
						}
					}
					
					//sleep(1);
			
					for(i=0;i<NBMAX && (strcmp(s->Tab[i].NomUtilisateur,M.Selection2 !=0));i++);
					Trace("i = %d, NBMAX = %d",i,NBMAX);
					if(i<NBMAX)
					{
						for(j=0;j<4 && Personnels[j].Personne != s->Tab[i].idPid;j++);
						Trace("j = %d, idPid = %d, Pidtab = %d",j,s->Tab[i].idPid,Personnels[j].Personne);
						if(j<4)
						{
							//kill(Personnels[j].Process,SIGUSR1);
							M.Type = Personnels[j].Process;
							Trace("Process : %d",Personnels[j].Process);
							if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
							{ perror("Err. de msgsnd()");
								exit(1);
							}
							sleep(1);
							M.Type = 1L;
						}
					
					}
				
				break;
			
			case EVOLUTIONTRAVAIL:
				Trace("Message EVOLUTIONTRAVAIL");
				for(i=0;i<5;i++)
				{
					if(s->Affichage[i]!=0)
					{
						kill(s->Affichage[i],SIGUSR1);
						M.Type = s->Affichage[i];
						if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
						{ perror("Err. de msgsnd()");
							exit(1);
						}
					}
				}
				sleep(1);
				break;
			
			case TRAVAILTERMINER:
				Trace("Message TRAVAILTERMINER");
				for(i=0;i<5;i++)
				{
					if(s->Affichage[i]!=0)
					{
						kill(s->Affichage[i],SIGUSR1);
						M.Type = s->Affichage[i];
						if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
						{ perror("Err. de msgsnd()");
							exit(1);
						}
					}
				}
				M.Type = 1L;
				sleep(1);
				kill(s->Administrateur1,SIGUSR1);
				if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
				{ perror("Err. de msgsnd()");
					exit(1);
				}
				sleep(1);
				
				break;
			
			case SUPPRESSIONTRAVAIL:
				Trace("Message SUPPRESSIONTRAVAIL");
				Trace("Lu : %s --- %s",M.Selection1,M.Selection2);
				for(i=0;i<5;i++)
				{
					if(s->Affichage[i]!=0)
					{
						kill(s->Affichage[i],SIGUSR1);
						M.Type = s->Affichage[i];
						if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
						{ perror("Err. de msgsnd()");
							exit(1);
						}
					}
				}
				break;
			}
	AfficheTab();
}

// Fin du processus Serveur1
}

void AfficheTab()
{
	int i = 0;
	printf("\tpid du Serveur1 :        %d\n",s->Serveur1);
	printf("\tpid du Serveur2 :      %d\n",s->Serveur2);
	printf("\tpid du Administrateur1 : %d\n",s->Administrateur1);
	printf("\tpid du ServiceCommande : %d\n",s->ServiceCommande);
	printf("\tpid du ServiceLivraison : %d\n",ServiceLiv);
//	printf("\tpid du Affichage :       %d\n",s->Affichage[0]);
	for(i=0;i<5;i++)
	{
		printf("\tpid du Affichage [%d]:       %d\n",i,s->Affichage[i]);
	}
	printf("\tTab Utilisateurs:\n");
	while (i < NBMAX) 
	{  
		printf("\t(i : %d)",i); 
		printf("\t%5d -%s- \n",
		s->Tab[i].idPid,s->Tab[i].NomUtilisateur);
		i++;
	}
	printf("\tpid du Processus Personnel [0] :       %d\n",Personnels[0].Process);
	if(Personnels[0].Personne) printf("\tPersonnel lié [0] :       %d\n",Personnels[0].Personne);
	printf("\tpid du Processus Personnel [1] :       %d\n",Personnels[1].Process);
	if(Personnels[1].Personne) printf("\tPersonnel lié [1] :       %d\n",Personnels[1].Personne);
	printf("\tpid du Processus Personnel [2] :       %d\n",Personnels[2].Process);
	if(Personnels[2].Personne) printf("\tPersonnel lié [2] :       %d\n",Personnels[2].Personne);
	printf("\tpid du Processus Personnel [3] :       %d\n",Personnels[3].Process);
	if(Personnels[3].Personne) printf("\tPersonnel lié [3] :       %d\n",Personnels[3].Personne);
	return ;
}
