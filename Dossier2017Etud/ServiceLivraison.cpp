// processus ServiceLivraison
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "Fichier.ini"
#include "Ecran.h"

void HNouvelCommande(int);

int idMsg;
MESSAGE	M;
int hdPipe[2];
char	Buff[255];

int main(int argc, char* argv[])
{
	pid_t		Pid;
	int rc;
	sleep(1);
	// Recuperation des ressources
	hdPipe[0] = atoi(argv[1]);
	hdPipe[1] = atoi(argv[2]);
	if(close(hdPipe[1])) { Trace("Erreur de close pipe SL"); exit(0); } //fermeture côté inutile
	
	struct sigaction Action;
	Action.sa_handler=HNouvelCommande;
	sigemptyset(&Action.sa_mask);
	Action.sa_flags=0;
	sigaction(SIGINT,&Action,NULL);
	
	if ((idMsg = msgget(KEY,0)) == -1)
	{ perror("Err. de msgget()");
		exit(1);
	}
	
	while(1)
	{
		Trace("Attente écriture Pipe");
		if(rc = read(hdPipe[0],&M,sizeof(M)) < 0) //Read bloquant de base
			{ Trace("Erreur Read Pipe"); exit(0); }
		Trace("-------------->Debut Livraison");
	
		Trace("Livraison de 8 secondes");
		sleep(8);
		Trace("Livraison Terminee");
		M.Type = 1L;
		M.Requete = SUPPRESSIONTRAVAIL;
		if (msgsnd(idMsg,&M,sizeof(M)-8,0) == -1)
		{ perror("Err. de msgsnd()");
			exit(1);
		}
	}
	
}

void HNouvelCommande(int)
{
	close(hdPipe[1]);
	exit(0);
}
