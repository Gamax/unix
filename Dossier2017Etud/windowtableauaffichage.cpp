#include "windowtableauaffichage.h"
#include "ui_windowtableauaffichage.h"

#include "Fichier.ini"
#include "Ecran.h"

extern int Colonne;
extern int idMsg;
extern MESSAGE	M;
extern WindowTableauAffichage* w;

void HNouvelCommande(int);

WindowTableauAffichage::WindowTableauAffichage(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WindowTableauAffichage)
{
    ui->setupUi(this);
    move(Colonne,0);
    int i = 0;
    while (i < 3)
    { lineCommande[i] = new QLineEdit(this);
      lineCommande[i]->setGeometry(QRect(40,20 + i * 30,100,20));
      lineCommande[i]->setReadOnly(true);
      lineCommande[i + 3] = new QLineEdit(this);
      lineCommande[i + 3]->setGeometry(QRect(380,20 + i * 30,100,20));
      lineCommande[i + 3]->setReadOnly(true);
      
      linePersonnel[i] = new QLineEdit(this);
      linePersonnel[i]->setGeometry(QRect(150,20 + i * 30,100,20));
      linePersonnel[i]->setReadOnly(true);
      linePersonnel[i + 3] = new QLineEdit(this);
      linePersonnel[i + 3]->setGeometry(QRect(490,20 + i * 30,100,20));
      linePersonnel[i + 3]->setReadOnly(true);
      
      labelEtat[i] = new QLabel(this);
      labelEtat[i]->setGeometry(QRect(260,20 + i * 30,80,20));
		labelEtat[i + 3] = new QLabel(this);
      labelEtat[i + 3]->setGeometry(QRect(600,20 + i * 30,80,20));
      i++;
    }
    // Armement des signaux
    struct sigaction Action;
	Action.sa_handler=HNouvelCommande;
	sigemptyset(&Action.sa_mask);
	Action.sa_flags=0;
	sigaction(SIGUSR1,&Action,NULL);
   
}

WindowTableauAffichage::~WindowTableauAffichage()
{
    delete ui;
}
#include "FctUtilesAffichage.cpp"


void HNouvelCommande(int Sig)
{
int rc;
Trace("Reception d'une nouvelle commande (%d)",Sig);

if((rc = msgrcv(idMsg,&M,sizeof(M) - sizeof(long),getpid(),0)) == -1)
{
	perror("ERREUR: SIGUSR2 non recu\n");
	exit(1);
}

	switch(M.Requete)
	{
		case NEWCOMMANDE:
		Trace("Reception NEWCOMMANDE");
		if(!w->getCommande(0))
		{
			w->setCommande(0,M.Selection1);
			w->setPersonnel(0,M.Selection2);
		}
		else
		{
			if(!w->getCommande(1))
			{
				w->setCommande(1,M.Selection1);
				w->setPersonnel(1,M.Selection2);
			}
			else
			{
				if(!w->getCommande(2))
				{
					w->setCommande(2,M.Selection1);
					w->setPersonnel(2,M.Selection2);

				}
				else
				{
					if(!w->getCommande(3))
					{
						w->setCommande(3,M.Selection1);
						w->setPersonnel(3,M.Selection2);

					}
					else
					{
						if(!w->getCommande(4))
						{
							w->setCommande(4,M.Selection1);
							w->setPersonnel(4,M.Selection2);

						}
						else
						{
							if(!w->getCommande(5))
							{
								w->setCommande(5,M.Selection1);
								w->setPersonnel(5,M.Selection2);

							}
						}
					}
				}
			}
		}
		break;
		
	case TRAVAILTERMINER:
		Trace("Reception TRAVAILTERMINER");
		if(w->getCommande(0) && !strcmp(M.Selection2,w->getCommande(0)) )
		{
			w->setEtat(0,"En Livraison");
		}
		else
		{
			if(w->getCommande(1) && !strcmp(M.Selection2,w->getCommande(1)) )
			{
				w->setEtat(1,"En Livraison");
			}
			else
			{
				if(w->getCommande(2) && !strcmp(M.Selection2,w->getCommande(2)))
				{
					w->setEtat(2,"En Livraison");
				}
				else
				{
					if(w->getCommande(3) && !strcmp(M.Selection2,w->getCommande(3)))
					{
						w->setEtat(3,"En Livraison");
					}
					else
					{
						if(w->getCommande(4) && !strcmp(M.Selection2,w->getCommande(4)))
						{
							w->setEtat(4,"En Livraison");
						}
						else
						{
							if(w->getCommande(5) && !strcmp(M.Selection2,w->getCommande(5)))
							{
								w->setEtat(5,"En Livraison");
							}
						}
					}
				}
			}
		}
		break;
	case EVOLUTIONTRAVAIL:
		Trace("Reception EVOLUTIONTRAVAIL --%s--",M.Selection2);
		
		if(strcmp(M.Selection2,"En Livraison"))
		{
			if(w->getCommande(0) && !strcmp(M.Selection1,w->getCommande(0)))
			{
				w->setEtat(0,M.Selection2);
			}
			else
			{
				if(w->getCommande(1) && !strcmp(M.Selection1,w->getCommande(1)))
				{
					w->setEtat(1,M.Selection2);
				}
				else
				{
					if(w->getCommande(2) && !strcmp(M.Selection1,w->getCommande(2)))
					{
						w->setEtat(2,M.Selection2);
					}
					else
					{
						if(w->getCommande(3) && !strcmp(M.Selection1,w->getCommande(3)))
						{
							w->setEtat(3,M.Selection2);
						}
						else
						{
							if(w->getCommande(4) && !strcmp(M.Selection1,w->getCommande(4)))
							{
								w->setEtat(4,M.Selection2);
							}
							else
							{
								if(w->getCommande(5) && !strcmp(M.Selection1,w->getCommande(5)))
								{
									w->setEtat(5,M.Selection2);
								}
							}
						}
					}
				}
			}
		}
		break;
	case SUPPRESSIONTRAVAIL:
		Trace("Reception SUPPRESSIONTRAVAIL --%s--",M.Selection2);
		if(w->getCommande(0) && !strcmp(M.Selection2,w->getCommande(0)))
		{
			w->setEtat(0,"");
			w->setCommande(0,"");
			w->setPersonnel(0,"");
		}
		else
		{
			if(w->getCommande(1) && !strcmp(M.Selection2,w->getCommande(1)))
			{
				w->setEtat(1,"");
				w->setCommande(1,"");
				w->setPersonnel(1,"");
			}
			else
			{
				if(w->getCommande(2) && !strcmp(M.Selection2,w->getCommande(2)))
				{
					w->setEtat(2,"");
					w->setCommande(2,"");
					w->setPersonnel(2,"");
				}
				else
				{
					if(w->getCommande(3) && !strcmp(M.Selection2,w->getCommande(3)))
					{
						w->setEtat(3,"");
						w->setCommande(3,"");
						w->setPersonnel(3,"");
					}
					else
					{
						if(w->getCommande(4) && !strcmp(M.Selection2,w->getCommande(4)))
						{
							w->setEtat(4,"");
							w->setCommande(4,"");
							w->setPersonnel(4,"");
						}
						else
						{
							if(w->getCommande(5) && !strcmp(M.Selection2,w->getCommande(5)))
							{
								w->setEtat(5,"");
								w->setCommande(5,"");
								w->setPersonnel(5,"");
							}
						}
					}
				}
			}
		}
		break;
	}
	
}
