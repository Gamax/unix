#include "FctUtilesAdministrateur.h"

void WindowAdministrateur::setCommande(int i,const char *T)
{
if (!strlen(T))
	{
	lineCommande[i]->clear();
	return;
	}
lineCommande[i]->setText(T);
return;
}

const char* WindowAdministrateur::getCommande(int i)const
{
char* Buff = (char*)malloc(255);
if (lineCommande[i]->text().size())
	{
	strcpy(Buff,lineCommande[i]->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

void WindowAdministrateur::setDisponible(int i,const char *T)
{
if (!strlen(T))
	{
	lineDisponible[i]->clear();
	return;
	}
lineDisponible[i]->setText(T);
return;
}

const char* WindowAdministrateur::getDisponible(int i)const
{
char* Buff = (char*)malloc(255);
if (lineDisponible[i]->text().size())
	{
	strcpy(Buff,lineDisponible[i]->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

void WindowAdministrateur::setPersonnel(int i,const char *T)
{
if (!strlen(T))
	{
	linePersonnel[i]->clear();
	return;
	}
linePersonnel[i]->setText(T);
return;
}

const char* WindowAdministrateur::getPersonnel(int i)const
{
char* Buff = (char*)malloc(255);
if (linePersonnel[i]->text().size())
	{
	strcpy(Buff,linePersonnel[i]->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

void WindowAdministrateur::setSelectionCommande(const char *T)
{
if (!strlen(T))
	{
	ui->lineSelectionCommande->clear();
	return;
	}
ui->lineSelectionCommande->setText(T);
return;
}

const char* WindowAdministrateur::getSelectionCommande()const
{
char* Buff = (char*)malloc(255);
if (ui->lineSelectionCommande->text().size())
	{
	strcpy(Buff,ui->lineSelectionCommande->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

void WindowAdministrateur::setNomCommande(const char *T)
{
if (!strlen(T))
	{
	ui->lineNomCommande->clear();
	return;
	}
ui->lineNomCommande->setText(T);
return;
}

const char* WindowAdministrateur::getNomCommande()const
{
char* Buff = (char*)malloc(255);
if (ui->lineNomCommande->text().size())
	{
	strcpy(Buff,ui->lineNomCommande->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

void WindowAdministrateur::setSelectionPersonnel(const char *T)
{
if (!strlen(T))
	{
	ui->lineSelectionPersonnel->clear();
	return;
	}
ui->lineSelectionPersonnel->setText(T);
return;
}

const char* WindowAdministrateur::getSelectionPersonnel()const
{
char* Buff = (char*)malloc(255);
if (ui->lineSelectionPersonnel->text().size())
	{
	strcpy(Buff,ui->lineSelectionPersonnel->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

const char* WindowAdministrateur::getCommandeN(int i) const
{
char* Buff = (char*)malloc(255);
if (lineCommandeN[i]->text().size())
	{
	strcpy(Buff,lineCommandeN[i]->text().toStdString().c_str());
	return Buff;
	}
return NULL;
}

int WindowAdministrateur::getCommandeT(int i) const
{
if (lineCommandeT[i]->text().size())
	{
	return atoi(lineCommandeT[i]->text().toStdString().c_str());
	}
return -1;
}

void WindowAdministrateur::setCommandeN(int i,const char*T)
{
	if (!strlen(T))
	{
		lineCommandeN[i]->clear();
		return;
	}
	lineCommandeN[i]->setText(T);
	return;
}

void WindowAdministrateur::setCommandeT(int i,const char*T)
{
	if (!strlen(T))
	{
		lineCommandeT[i]->clear();
		return;
	}
	lineCommandeT[i]->setText(T);
	return;
}
