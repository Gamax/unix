#include "gestionannonce.h"
#include "ui_gestionannonce.h"

#include "Ecran.h"

GestionAnnonce::GestionAnnonce(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GestionAnnonce)
{
    ui->setupUi(this);
    
    if((fd = open("data.dat",O_RDWR | O_CREAT | O_EXCL,0666))==-1){
		if((fd = open("data.dat",O_RDWR))== -1)
			exit(-1);
		
	}
}

GestionAnnonce::~GestionAnnonce()
{
    delete ui;
}

void GestionAnnonce::on_ButtonAjouter_clicked()
{


Trace("on_ButtonAjouter_clicked\n");
printf("--%d--\n",getNumero());
printf("--%s--\n",getSituation());
printf("--%s--\n",getVille());
printf("--%d--\n",getPrix());
printf("--%s--\n",getContact());
}

void GestionAnnonce::on_ButtonModifier_clicked()
{
Trace("on_ButtonModifier_clicked\n");
setNumero(999);
setSituation("abc");
setVille("wxc");
setPrix(111);
setContact("moi");
}

void GestionAnnonce::on_ButtonSupprimer_clicked()
{
Trace("on_ButtonSupprimer_clicked\n");
}

void GestionAnnonce::on_ButtonRechercher_clicked()
{
Trace("on_ButtonRechercher_clicked\n");
}

void GestionAnnonce::on_ButtonSuivant_clicked()
{
Trace("on_ButtonSuivant_clicked\n");
}

void GestionAnnonce::on_ButtonTerminer_clicked()
{
Trace("on_ButtonTerminer_clicked\n");
exit(0);
}

void GestionAnnonce::setNumero(int Num)
{
char	Buffer[20];
sprintf(Buffer,"%d",Num);
ui->lineNumero->setText(Buffer);
}

int GestionAnnonce::getNumero() const
{
if (ui->lineNumero->text().size())
	return atoi(ui->lineNumero->text().toStdString().c_str());
return -1;
}

void GestionAnnonce::setSituation(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineSituation->clear();
   return;
   }
ui->lineSituation->setText(Buffer);
}

const char* GestionAnnonce::getSituation() const
{
if (ui->lineSituation->text().size())
	return ui->lineSituation->text().toStdString().c_str();
return NULL;
}

void GestionAnnonce::setVille(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineVille->clear();
   return;
   }
ui->lineVille->setText(Buffer);
}

const char* GestionAnnonce::getVille() const
{
if (ui->lineVille->text().size())
	return ui->lineVille->text().toStdString().c_str();
return NULL;
}

void GestionAnnonce::setPrix(int Prix)
{
char	Buffer[20];
sprintf(Buffer,"%d",Prix);
ui->linePrix->setText(Buffer);
}

int GestionAnnonce::getPrix() const
{
if (ui->linePrix->text().size())
	return atoi(ui->linePrix->text().toStdString().c_str());
return -1;
}

void GestionAnnonce::setContact(const char *Buffer)
{
if (strlen(Buffer) == 0 ) 
   { 
   ui->lineContact->clear();
   return;
   }
ui->lineContact->setText(Buffer);
}

const char* GestionAnnonce::getContact() const
{
if (ui->lineContact->text().size())
	return ui->lineContact->text().toStdString().c_str();
return NULL;
}
