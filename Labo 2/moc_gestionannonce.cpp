/****************************************************************************
** Meta object code from reading C++ file 'gestionannonce.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "gestionannonce.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gestionannonce.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GestionAnnonce[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      43,   15,   15,   15, 0x08,
      71,   15,   15,   15, 0x08,
     100,   15,   15,   15, 0x08,
     130,   15,   15,   15, 0x08,
     157,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_GestionAnnonce[] = {
    "GestionAnnonce\0\0on_ButtonAjouter_clicked()\0"
    "on_ButtonModifier_clicked()\0"
    "on_ButtonSupprimer_clicked()\0"
    "on_ButtonRechercher_clicked()\0"
    "on_ButtonSuivant_clicked()\0"
    "on_ButtonTerminer_clicked()\0"
};

void GestionAnnonce::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GestionAnnonce *_t = static_cast<GestionAnnonce *>(_o);
        switch (_id) {
        case 0: _t->on_ButtonAjouter_clicked(); break;
        case 1: _t->on_ButtonModifier_clicked(); break;
        case 2: _t->on_ButtonSupprimer_clicked(); break;
        case 3: _t->on_ButtonRechercher_clicked(); break;
        case 4: _t->on_ButtonSuivant_clicked(); break;
        case 5: _t->on_ButtonTerminer_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData GestionAnnonce::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GestionAnnonce::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_GestionAnnonce,
      qt_meta_data_GestionAnnonce, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GestionAnnonce::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GestionAnnonce::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GestionAnnonce::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GestionAnnonce))
        return static_cast<void*>(const_cast< GestionAnnonce*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int GestionAnnonce::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
