#ifndef GESTIONANNONCE_H
#define GESTIONANNONCE_H

#include <QMainWindow>

typedef struct{
		int numero;
		char situation[20];
		char ville[20];
		int prix;
		char contact[20];
} bien;

namespace Ui {
class GestionAnnonce;
}

class GestionAnnonce : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit GestionAnnonce(QWidget *parent = 0);
    ~GestionAnnonce();
    void 	setNumero(int);
    int 		getNumero() const;
    void		setSituation(const char*);
    const char* getSituation() const;
    void		setVille(const char*);
    const char* getVille() const;
    void 	setPrix(int);
    int 		getPrix() const;
    void		setContact(const char*);
    const char* getContact() const;

private slots:
    void on_ButtonAjouter_clicked();
    void on_ButtonModifier_clicked();
    void on_ButtonSupprimer_clicked();
    void on_ButtonRechercher_clicked();
    void on_ButtonSuivant_clicked();
    void on_ButtonTerminer_clicked();
private:
    Ui::GestionAnnonce *ui;
    int fd; //file descriptor
};

#endif // GESTIONANNONCE_H
