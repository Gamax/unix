/********************************************************************************
** Form generated from reading UI file 'gestionannonce.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GESTIONANNONCE_H
#define UI_GESTIONANNONCE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GestionAnnonce
{
public:
    QWidget *centralWidget;
    QLabel *labelNumero;
    QLabel *labelSituation;
    QLabel *labelPrix;
    QLabel *labelContact;
    QPushButton *ButtonAjouter;
    QPushButton *ButtonModifier;
    QPushButton *ButtonSupprimer;
    QPushButton *ButtonRechercher;
    QPushButton *ButtonSuivant;
    QPushButton *ButtonTerminer;
    QLineEdit *lineNumero;
    QLineEdit *lineSituation;
    QLineEdit *linePrix;
    QLineEdit *lineContact;
    QLabel *labelVille;
    QLineEdit *lineVille;
    QMenuBar *menuBar;
    QMenu *menuGestion_de_bien_Louer;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GestionAnnonce)
    {
        if (GestionAnnonce->objectName().isEmpty())
            GestionAnnonce->setObjectName(QString::fromUtf8("GestionAnnonce"));
        GestionAnnonce->resize(400, 251);
        centralWidget = new QWidget(GestionAnnonce);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        labelNumero = new QLabel(centralWidget);
        labelNumero->setObjectName(QString::fromUtf8("labelNumero"));
        labelNumero->setGeometry(QRect(20, 20, 70, 16));
        labelSituation = new QLabel(centralWidget);
        labelSituation->setObjectName(QString::fromUtf8("labelSituation"));
        labelSituation->setGeometry(QRect(20, 50, 70, 13));
        labelPrix = new QLabel(centralWidget);
        labelPrix->setObjectName(QString::fromUtf8("labelPrix"));
        labelPrix->setGeometry(QRect(20, 110, 70, 13));
        labelContact = new QLabel(centralWidget);
        labelContact->setObjectName(QString::fromUtf8("labelContact"));
        labelContact->setGeometry(QRect(20, 140, 70, 13));
        ButtonAjouter = new QPushButton(centralWidget);
        ButtonAjouter->setObjectName(QString::fromUtf8("ButtonAjouter"));
        ButtonAjouter->setGeometry(QRect(280, 20, 75, 23));
        ButtonModifier = new QPushButton(centralWidget);
        ButtonModifier->setObjectName(QString::fromUtf8("ButtonModifier"));
        ButtonModifier->setGeometry(QRect(280, 50, 75, 23));
        ButtonSupprimer = new QPushButton(centralWidget);
        ButtonSupprimer->setObjectName(QString::fromUtf8("ButtonSupprimer"));
        ButtonSupprimer->setGeometry(QRect(280, 80, 75, 23));
        ButtonRechercher = new QPushButton(centralWidget);
        ButtonRechercher->setObjectName(QString::fromUtf8("ButtonRechercher"));
        ButtonRechercher->setGeometry(QRect(280, 110, 75, 23));
        ButtonSuivant = new QPushButton(centralWidget);
        ButtonSuivant->setObjectName(QString::fromUtf8("ButtonSuivant"));
        ButtonSuivant->setGeometry(QRect(20, 170, 75, 23));
        ButtonTerminer = new QPushButton(centralWidget);
        ButtonTerminer->setObjectName(QString::fromUtf8("ButtonTerminer"));
        ButtonTerminer->setGeometry(QRect(280, 170, 75, 23));
        lineNumero = new QLineEdit(centralWidget);
        lineNumero->setObjectName(QString::fromUtf8("lineNumero"));
        lineNumero->setGeometry(QRect(100, 20, 150, 20));
        lineSituation = new QLineEdit(centralWidget);
        lineSituation->setObjectName(QString::fromUtf8("lineSituation"));
        lineSituation->setGeometry(QRect(100, 50, 150, 20));
        linePrix = new QLineEdit(centralWidget);
        linePrix->setObjectName(QString::fromUtf8("linePrix"));
        linePrix->setGeometry(QRect(100, 110, 150, 20));
        lineContact = new QLineEdit(centralWidget);
        lineContact->setObjectName(QString::fromUtf8("lineContact"));
        lineContact->setGeometry(QRect(100, 140, 150, 20));
        labelVille = new QLabel(centralWidget);
        labelVille->setObjectName(QString::fromUtf8("labelVille"));
        labelVille->setGeometry(QRect(20, 80, 70, 13));
        lineVille = new QLineEdit(centralWidget);
        lineVille->setObjectName(QString::fromUtf8("lineVille"));
        lineVille->setGeometry(QRect(100, 80, 150, 20));
        GestionAnnonce->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GestionAnnonce);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        menuGestion_de_bien_Louer = new QMenu(menuBar);
        menuGestion_de_bien_Louer->setObjectName(QString::fromUtf8("menuGestion_de_bien_Louer"));
        GestionAnnonce->setMenuBar(menuBar);
        statusBar = new QStatusBar(GestionAnnonce);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        GestionAnnonce->setStatusBar(statusBar);

        menuBar->addAction(menuGestion_de_bien_Louer->menuAction());

        retranslateUi(GestionAnnonce);

        QMetaObject::connectSlotsByName(GestionAnnonce);
    } // setupUi

    void retranslateUi(QMainWindow *GestionAnnonce)
    {
        GestionAnnonce->setWindowTitle(QApplication::translate("GestionAnnonce", "Agence Immo", 0, QApplication::UnicodeUTF8));
        labelNumero->setText(QApplication::translate("GestionAnnonce", "Num\303\251ro:", 0, QApplication::UnicodeUTF8));
        labelSituation->setText(QApplication::translate("GestionAnnonce", "Situation:", 0, QApplication::UnicodeUTF8));
        labelPrix->setText(QApplication::translate("GestionAnnonce", "Prix:", 0, QApplication::UnicodeUTF8));
        labelContact->setText(QApplication::translate("GestionAnnonce", "Contact :", 0, QApplication::UnicodeUTF8));
        ButtonAjouter->setText(QApplication::translate("GestionAnnonce", "Ajouter", 0, QApplication::UnicodeUTF8));
        ButtonModifier->setText(QApplication::translate("GestionAnnonce", "Modifier", 0, QApplication::UnicodeUTF8));
        ButtonSupprimer->setText(QApplication::translate("GestionAnnonce", "Supprimer", 0, QApplication::UnicodeUTF8));
        ButtonRechercher->setText(QApplication::translate("GestionAnnonce", "Rechercher", 0, QApplication::UnicodeUTF8));
        ButtonSuivant->setText(QApplication::translate("GestionAnnonce", "Suivant", 0, QApplication::UnicodeUTF8));
        ButtonTerminer->setText(QApplication::translate("GestionAnnonce", "Terminer", 0, QApplication::UnicodeUTF8));
        labelVille->setText(QApplication::translate("GestionAnnonce", "Ville:", 0, QApplication::UnicodeUTF8));
        menuGestion_de_bien_Louer->setTitle(QApplication::translate("GestionAnnonce", "Gestion de bien \303\240 Louer", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GestionAnnonce: public Ui_GestionAnnonce {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GESTIONANNONCE_H
